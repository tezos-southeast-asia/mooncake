
module Mooncake.Common.Config where

defaultPage :: Int
defaultPage = 0

defaultNumberPerPage :: Int
defaultNumberPerPage = 10

maxNumberPerPage :: Int
maxNumberPerPage = 50

safeNumberPerPage :: Int  -- ^ form user input
                  -> Int
safeNumberPerPage u | u > maxNumberPerPage = maxNumberPerPage
                    | otherwise            = u

offset :: Int -> Int -> Int
offset p n = p * n

gateAddress :: String
gateAddress = "KT1GZ2186Kuarw43NDyJ8baoDZU9gkyL8tWU" 

tezosNodeURL :: String
tezosNodeURL = "http://node2.sg.tezos.org.sg:8732"

tezosNodeURL_HTTPS :: (String, String)
tezosNodeURL_HTTPS = ("alphanet.api.tezos.id", "443")
