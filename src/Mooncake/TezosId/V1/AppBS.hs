{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE TemplateHaskell            #-}

module Mooncake.TezosId.V1.AppBS
  ( latestBlocks'
  , blocksNum
  , blockDetail
  , lastesOps
  , opsNum
  , opDetail
  , lastesTxs
  , txsNum
  , lastesEndors
  , endorsNum
  , lastesDelegations
  , delegationsNum
  , lastesOriginations
  , originationsNum
  , lastesBallots
  , ballotsNum
  , lastesDoubleEndorsementEvidences
  , doubleEndorsementEvidencesNum
  , lastesDoubleBakingEvidences
  , doubleBakingEvidencesNum
  , lastesSeedNonceEvelations
  , seedNonceEvelationsNum
  , lastesReveals
  , revealsNum
  , lastesActivateAccounts
  , activateAccountsNum
  , lastesProposals
  , proposalsNum
  , lastesProposers
  , proposersNum
  , lastesProtocols
  , protocolsNum
  , twentyFourStat
  , accountLists
  , accountsNum
  , tulipToolsTotal'
  , tulipToolsCirculating'
  ) where

import Mooncake.TezosId.V1.DBModels
import Mooncake.Common.DB
import Mooncake.Common.Config
import Mooncake.Common.UtilityTH

import Control.Monad.IO.Class  (liftIO)

import Data.List
import Data.Char
import Data.Maybe
import Data.Pool
import qualified Data.Text as T

import Database.Groundhog as G
import Database.Groundhog.Core as G
import Database.Groundhog.Postgresql as G

import Data.Time.Clock (getCurrentTime, UTCTime, addUTCTime)
import Data.Time.Format (formatTime, defaultTimeLocale)

isAllPersistNull :: [[PersistValue]] -> [[PersistValue]]
isAllPersistNull = filter (any (/= PersistNull))

tulipToolsTotal' :: Pool Postgresql
                 -> IO (Maybe TulipToolsTotal)
tulipToolsTotal' pool = do
  let table = "s1"
      fields = strFieldNamesInScope (undefined :: TulipToolsTotal) table
      query = unwords [ "select " <> fields
                      , " from tulip_tools_total as " <> table
                      , " order by _inserted_timestamp desc limit 1"
                      ]
  liftIO $ flip G.runDbConn pool
         $ raw_ query []
         $ \xs -> do
             (v, _) <- fromEntity xs
             return $ listToMaybe v

tulipToolsCirculating' :: Pool Postgresql
                       -> IO (Maybe TulipToolsCirculating)
tulipToolsCirculating' pool = do
  let table = "s1"
      fields = strFieldNamesInScope (undefined :: TulipToolsCirculating) table
      query = unwords [ "select " <> fields
                      , " from tulip_tools_circulating as " <> table
                      , " order by _inserted_timestamp desc limit 1"
                      ]
  liftIO $ flip G.runDbConn pool
         $ raw_ query []
         $ \xs -> do
             (v, _) <- fromEntity xs
             return $ listToMaybe v

latestBlocks' :: Pool Postgresql
              -> Int      -- ^ Page
              -> Int      -- ^ Number
              -> IO [ BlockDetail ]
latestBlocks' pool p n = do
  let boTable = "s1"
      baTable = "s2"
      bsTable = "s3"
      boFields = strFieldNamesInScope (undefined :: Blocks) boTable
      baFields = strFieldNamesInScope (undefined :: BlockAlphas) baTable
      bsFields = strFieldNamesInScope (undefined :: BlockStats) bsTable
      sn = safeNumberPerPage n
      pn = PersistInt64 $ fromIntegral sn
      po = PersistInt64 $ fromIntegral $ offset p sn
      query = unwords
                [ "with mini_block as (select * from blocks order by level desc limit ? offset ?)"
                , " select " <> boFields <> ", " <> baFields <>  ", " <> bsFields
                , " from mini_block"
                  , " as " <> boTable <> " "
                , " left join block_alphas " <> baTable <> " on " <> boTable <> ".uuid = " <> baTable <> ".block_uuid"
                , " left join block_stats " <> bsTable <> " on " <> bsTable <> ".level = " <> baTable <> ".level"
                , " order by " <> boTable <> ".level desc"
                ]

  liftIO $ flip G.runDbConn pool $ raw_ query [ pn, po ]
         $ \xs -> do
                    (v1, r1) <- fromEntity xs
                    (v2, r2) <- fromEntity r1
                    (v3, _) <- fromEntity r2
                    let uncurry' = $(uncurryN 3)
                    return $ map (uncurry' L) $ zip3 v1 v2 v3

blocksNum :: Pool Postgresql -> IO Int
blocksNum pool =
  liftIO $ flip G.runDbConn pool $ countAll (undefined :: Blocks)

blockDetail :: Pool Postgresql -> String -> IO (Maybe BlockDetail)
blockDetail pool q = do
  let boTable = "s1"
      baTable = "s2"
      bsTable = "s3"
      boFields = strFieldNamesInScope (undefined :: Blocks) boTable
      baFields = strFieldNamesInScope (undefined :: BlockAlphas) baTable
      bsFields = strFieldNamesInScope (undefined :: BlockStats) bsTable
      whereCauseField = if Prelude.all isDigit q then
                          ".level=?"
                        else
                          ".hash=?"
      whereCauseValue = if Prelude.all isDigit q then
                          PersistInt64 $ read q
                        else
                          PersistText $ T.pack q

      query = unwords
                [ "with mini_block as (select * from blocks where blocks " <> whereCauseField <> ")"
                , " select " <> boFields <> ", " <> baFields <>  ", " <> bsFields
                , " from mini_block"
                  , " as " <> boTable <> " "
                , " left join block_alphas " <> baTable <> " on " <> boTable <> ".uuid = " <> baTable <> ".block_uuid"
                , " left join block_stats " <> bsTable <> " on " <> bsTable <> ".level = " <> baTable <> ".level"
                ]
  liftIO $ flip G.runDbConn pool $ raw_ query [ whereCauseValue ]
         $ \xs -> do
                    (v1, r1) <- fromEntity xs
                    (v2, r2) <- fromEntity r1
                    (v3, _) <- fromEntity r2
                    let uncurry' = $(uncurryN 3)
                    return $ listToMaybe $ map (uncurry' L) $ zip3 v1 v2 v3

lastesOps :: Pool Postgresql
          -> Int      -- ^ Page
          -> Int      -- ^ Number
          -> IO [ Ops ]
lastesOps pool p n = do
  let opTable = "s1"
      opFields = strFieldNamesInScope (undefined :: Ops) opTable
      sn = safeNumberPerPage n
      pn = PersistInt64 $ fromIntegral sn
      po = PersistInt64 $ fromIntegral $ offset p sn
      query = unwords
                [ " select " <> opFields <> " from ops as " <> opTable
                , " where block_level is not null"
                , " order by block_level desc, uuid asc"
                , " limit ? offset ?"
                ]

  liftIO $ flip G.runDbConn pool $ raw_ query [ pn, po ]
         $ \xs -> do
                    (v, _) <- fromEntity xs
                    return v

opsNum :: Pool Postgresql -> IO Int
opsNum pool = do
  let query = unwords
                [ " select count(*) from ops"
                , " where block_level is not null"
                ]
  liftIO $ flip G.runDbConn pool $ raw_ query []
         $ \xs -> fromSinglePersistValue $ head $ head xs

opDetail :: Pool Postgresql -> String -> IO (Maybe Ops)
opDetail pool q = do
  let opTable = "s1"
      opFields = strFieldNamesInScope (undefined :: Ops) opTable
      h = PersistText $ T.pack q
      query = unwords
                [ " select " <> opFields <> " from ops as " <> opTable
                , " where op_hash = ?"
                ]

  liftIO $ flip G.runDbConn pool $ raw_ query [ h ]
         $ \xs -> do
                    (v, _) <- fromEntity xs
                    return $ listToMaybe v

-- Internal Delegations

internalDelegations
  :: Pool Postgresql
  -> T.Text         -- ^ op_delegations.metadata_uuid
  -> IO [ InternalOpDelegations ]
internalDelegations pool u = do
  let delegationsTable = "s1"
      internalDelegationsTable = "s2"
      internalDelegationsFields = strFieldNamesInScope (undefined :: InternalOpDelegations) internalDelegationsTable
      query = unwords
        [ " select " <> internalDelegationsFields
        , " from op_txs as " <> delegationsTable
        , " left join internal_op_delegations " <> internalDelegationsTable
        , " on " <> delegationsTable <> ".metadata_uuid =" <> internalDelegationsTable <> ".op_metadata_uuid"
        , " where " <> delegationsTable <> ".metadata_uuid = ?"
        ]
  liftIO $ flip G.runDbConn pool $ raw_ query [ PersistText u ]
         $ \xs -> do
                    (v, _) <- fromEntity $ isAllPersistNull xs
                    return v


-- Internal Txs

internalTxs
  :: Pool Postgresql
  -> T.Text         -- ^ op_txs.metadata_uuid
  -> IO [ InternalOpTxs ]
internalTxs pool u = do
  let txsTable = "s1"
      internalTxsTable = "s2"
      internalTxsFields = strFieldNamesInScope (undefined :: InternalOpTxs) internalTxsTable
      query = unwords
        [ " select " <> internalTxsFields
        , " from op_txs as " <> txsTable
        , " left join internal_op_txs " <> internalTxsTable
        , " on " <> txsTable <> ".metadata_uuid =" <> internalTxsTable <> ".op_metadata_uuid"
        , " where " <> txsTable <> ".metadata_uuid = ?"
        ]
  liftIO $ flip G.runDbConn pool $ raw_ query [ PersistText u ]
         $ \xs -> do
                    (v, _) <- fromEntity $ isAllPersistNull xs
                    return v

--- Txs

lastesTxs :: Pool Postgresql
           -> Int      -- ^ Page
           -> Int      -- ^ Number
           -> Maybe String -- ^ account
           -> Maybe String -- ^ block
           -> Maybe String -- ^ op
           -> IO [ OpTxsDetail ]
lastesTxs pool p n ma mb Nothing = do
  let txsTable = "s1"
      opsTable = "s2"
      txsFields = strFieldNamesInScope (undefined :: OpTxs) txsTable
      opsFields = strFieldNamesInScope (undefined :: Ops) opsTable
      sn = safeNumberPerPage n
      pn = PersistInt64 $ fromIntegral sn
      po = PersistInt64 $ fromIntegral $ offset p sn
      (blockSql, blockParamSql, blockCheck) = checkBlock mb Nothing
      (accSql, accParamSql, accCheck) = checkTxAcc ma
      query = unwords
                [ " select " <> opsFields <> "," <> txsFields
                , " from "
                  , " (select " <> txsFields <> " from op_txs as " <> txsTable
                    , " where block_level is not null"
                    , " " <> blockSql
                    , " " <> accSql
                    , " order by block_level desc, uuid asc"
                    , " limit ? offset ?) as " <> txsTable
                , " left join ops " <> opsTable
                , " on " <> opsTable <> ".uuid = " <> txsTable <> ".op_uuid"
                ]
  (rOps, rTxs) <- if accCheck && blockCheck then
    liftIO $ flip G.runDbConn pool
             $ raw_ query (blockParamSql <> accParamSql <> [ pn, po ])
             $ \xs -> do
                 (v1, r1) <- fromEntity xs
                 (v2, _) <- fromEntity r1
                 return (v1, v2)
  else
    return ([], [])

  let getMetaUuid (OpTxs _ _ _ _ _ _ _ u _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _) = u
      metaUuids = map getMetaUuid $ rTxs
      uncurry' = $(uncurryN 4)
  iTxs <- mapM (internalTxs pool . fromMaybe "") metaUuids
  iDelegations <- mapM (internalDelegations pool . fromMaybe "") metaUuids
  return $ map (uncurry' T) $ zip4 rOps rTxs iTxs iDelegations
lastesTxs pool p n ma mb (Just o) = do
  let txsTable = "s1"
      opsTable = "s2"
      txsFields = strFieldNamesInScope (undefined :: OpTxs) txsTable
      opsFields = strFieldNamesInScope (undefined :: Ops) opsTable
      sn = safeNumberPerPage n
      pn = PersistInt64 $ fromIntegral sn
      po = PersistInt64 $ fromIntegral $ offset p sn
      (blockSql, blockParamSql, blockCheck) = checkBlock mb (Just txsTable)
      (accSql, accParamSql, accCheck) = checkTxAcc ma
      (opParamSql, opCheck) = checkOp' o
      query = unwords
                [ " select " <> opsFields <> "," <> txsFields
                , " from "
                  , "(select * from ops where ops.op_hash = ?) as " <> opsTable
                , " inner join op_txs " <> txsTable
                , " on " <> opsTable <> ".uuid = " <> txsTable <> ".op_uuid"
                , " where " <> txsTable <> ".block_level is not null"
                  , " " <> blockSql
                  , " " <> accSql
                , " order by " <> txsTable <> ".block_level desc, " <> txsTable <> ".uuid asc"
                , " limit ? offset ?"
                ]
  (rOps, rTxs) <- if accCheck && opCheck && blockCheck then
    liftIO $ flip G.runDbConn pool
           $ raw_ query (opParamSql <> blockParamSql <> accParamSql <> [ pn, po ])
           $ \xs -> do
                    (v1, r1) <- fromEntity xs
                    (v2, _) <- fromEntity r1
                    return (v1, v2)
  else
    return ([], [])

  let getMetaUuid (OpTxs _ _ _ _ _ _ _ u _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _) = u
      uuids = map getMetaUuid $ rTxs
      uncurry' = $(uncurryN 4)
  iTxs <- mapM (internalTxs pool . fromMaybe "") uuids
  iDelegations <- mapM (internalDelegations pool . fromMaybe "") uuids
  return $ map (uncurry' T) $ zip4 rOps rTxs iTxs iDelegations

txsNum :: Pool Postgresql -> Maybe String -> Maybe String -> Maybe String -> IO Int
txsNum pool ma mb Nothing = do
  let (blockSql, blockParamSql, blockCheck) = checkBlock mb Nothing
      (accSql, accParamSql, accCheck) = checkTxAcc ma
      query = unwords
                [ " select count(*) from op_txs"
                , " where block_level is not null"
                  , " " <> blockSql
                  , " " <> accSql
                ]
  if accCheck && blockCheck then
    liftIO $ flip G.runDbConn pool $ raw_ query (blockParamSql <> accParamSql)
           $ \xs -> fromSinglePersistValue $ head $ head xs
  else
    return 0
txsNum pool ma mb (Just o) = do
  let txsTable = "s1"
      opsTable = "s2"
      (blockSql, blockParamSql, blockCheck) = checkBlock mb (Just txsTable)
      (accSql, accParamSql, accCheck) = checkTxAcc ma
      (opParamSql, opCheck) = checkOp' o
      query = unwords
                [ " select count(*)"
                , " from "
                  , "(select * from ops where ops.op_hash = ?) as " <> opsTable
                , " inner join op_txs " <> txsTable
                , " on " <> opsTable <> ".uuid = " <> txsTable <> ".op_uuid"
                , " where " <> txsTable <> ".block_level is not null"
                  , " " <> blockSql
                  , " " <> accSql
                ]
  if accCheck && blockCheck && opCheck then
    liftIO $ flip G.runDbConn pool
           $ raw_ query (opParamSql <> blockParamSql <> accParamSql)
           $ \xs -> fromSinglePersistValue $ head $ head xs
  else
    return 0
---

lastesEndors :: Pool Postgresql
           -> Int      -- ^ Page
           -> Int      -- ^ Number
           -> Maybe String -- ^ account
           -> Maybe String -- ^ block
           -> Maybe String -- ^ op
           -> IO [ OpEndorsDetail ]
lastesEndors pool p n ma mb Nothing = do
  let endorTable = "s1"
      opsTable = "s2"
      endorFields = strFieldNamesInScope (undefined :: OpEndorments) endorTable
      opsFields = strFieldNamesInScope (undefined :: Ops) opsTable
      sn = safeNumberPerPage n
      pn = PersistInt64 $ fromIntegral sn
      po = PersistInt64 $ fromIntegral $ offset p sn
      (blockSql, blockParamSql, blockCheck) = checkBlock mb Nothing
      (accSql, accParamSql, accCheck) = checkEndorAcc ma
      query = unwords
                [ " select " <> opsFields <> "," <> endorFields
                , " from "
                  , " (select " <> endorFields <> " from op_endorsements as " <> endorTable
                    , " where block_level is not null"
                    , " " <> blockSql
                    , " " <> accSql
                    , " order by block_level desc, uuid asc"
                    , " limit ? offset ?) as " <> endorTable
                , " left join ops " <> opsTable
                , " on " <> opsTable <> ".uuid = " <> endorTable <> ".op_uuid"
                ]
  if accCheck && blockCheck then
    liftIO $ flip G.runDbConn pool
           $ raw_ query (blockParamSql <> accParamSql <> [ pn, po ])
           $ \xs -> do
               (v1, r1) <- fromEntity xs
               (v2, _) <- fromEntity r1
               return $ zipWith E v1 v2
  else
    return []
lastesEndors pool p n ma mb (Just o) = do
  let endorTable = "s1"
      opsTable = "s2"
      endorFields = strFieldNamesInScope (undefined :: OpEndorments) endorTable
      opsFields = strFieldNamesInScope (undefined :: Ops) opsTable
      sn = safeNumberPerPage n
      pn = PersistInt64 $ fromIntegral sn
      po = PersistInt64 $ fromIntegral $ offset p sn
      (blockSql, blockParamSql, blockCheck) = checkBlock mb (Just endorTable)
      (accSql, accParamSql, accCheck) = checkEndorAcc ma
      (opParamSql, opCheck) = checkOp' o
      query = unwords
                [ " select " <> opsFields <> "," <> endorFields
                , " from "
                  , "(select * from ops where ops.op_hash = ?) as " <> opsTable
                , " inner join op_endorsements " <> endorTable
                , " on " <> opsTable <> ".uuid = " <> endorTable <> ".op_uuid"
                , " where " <> endorTable <> ".block_level is not null"
                  , " " <> blockSql
                  , " " <> accSql
                , " order by " <> endorTable <> ".block_level desc, " <> endorTable <> ".uuid asc"
                , " limit ? offset ?"
                ]
  if accCheck && opCheck && blockCheck then
    liftIO $ flip G.runDbConn pool
           $ raw_ query (opParamSql <> blockParamSql <> accParamSql <> [ pn, po ])
           $ \xs -> do
                    (v1, r1) <- fromEntity xs
                    (v2, _) <- fromEntity r1
                    return $ zipWith E v1 v2
  else
    return []

endorsNum :: Pool Postgresql -> Maybe String -> Maybe String -> Maybe String -> IO Int
endorsNum pool ma mb Nothing = do
  let (blockSql, blockParamSql, blockCheck) = checkBlock mb Nothing
      (accSql, accParamSql, accCheck) = checkEndorAcc ma
      query = unwords
                [ " select count(*) from op_endorsements"
                , " where block_level is not null"
                  , " " <> blockSql
                  , " " <> accSql
                ]
  if accCheck && blockCheck then
    liftIO $ flip G.runDbConn pool $ raw_ query (blockParamSql <> accParamSql)
           $ \xs -> fromSinglePersistValue $ head $ head xs
  else
    return 0
endorsNum pool ma mb (Just o) = do
  let endorTable = "s1"
      opsTable = "s2"
      (blockSql, blockParamSql, blockCheck) = checkBlock mb (Just endorTable)
      (accSql, accParamSql, accCheck) = checkEndorAcc ma
      (opParamSql, opCheck) = checkOp' o
      query = unwords
                [ " select count(*)"
                , " from "
                  , "(select * from ops where ops.op_hash = ?) as " <> opsTable
                , " inner join op_endorsements " <> endorTable
                , " on " <> opsTable <> ".uuid = " <> endorTable <> ".op_uuid"
                , " where " <> endorTable <> ".block_level is not null"
                  , " " <> blockSql
                  , " " <> accSql
                ]
  if accCheck && blockCheck && opCheck then
    liftIO $ flip G.runDbConn pool
           $ raw_ query (opParamSql <> blockParamSql <> accParamSql)
           $ \xs -> fromSinglePersistValue $ head $ head xs
  else
    return 0

--- Delegations

lastesDelegations :: Pool Postgresql
           -> Int      -- ^ Page
           -> Int      -- ^ Number
           -> Maybe String -- ^ account
           -> Maybe String -- ^ block
           -> Maybe String -- ^ op
           -> IO [ OpDelegationsDetail ]
lastesDelegations pool p n ma mb Nothing = do
  let delegationsTable = "s1"
      opsTable = "s2"
      delegationsFields = strFieldNamesInScope (undefined :: OpDelegations) delegationsTable
      opsFields = strFieldNamesInScope (undefined :: Ops) opsTable
      sn = safeNumberPerPage n
      pn = PersistInt64 $ fromIntegral sn
      po = PersistInt64 $ fromIntegral $ offset p sn
      (blockSql, blockParamSql, blockCheck) = checkBlock mb Nothing
      (accSql, accParamSql, accCheck) = checkDelegationAcc ma
      query = unwords
                [ " select " <> opsFields <> "," <> delegationsFields
                , " from "
                  , " (select " <> delegationsFields <> " from op_delegations as " <> delegationsTable
                    , " where block_level is not null"
                    , " " <> blockSql
                    , " " <> accSql
                    , " order by block_level desc, uuid asc"
                    , " limit ? offset ?) as " <> delegationsTable
                , " left join ops " <> opsTable
                , " on " <> opsTable <> ".uuid = " <> delegationsTable <> ".op_uuid"
                ]
  if accCheck && blockCheck then
    liftIO $ flip G.runDbConn pool
           $ raw_ query (blockParamSql <> accParamSql <> [ pn, po ])
           $ \xs -> do
               (v1, r1) <- fromEntity xs
               (v2, _) <- fromEntity r1
               return $ zipWith D v1 v2
  else
    return []
lastesDelegations pool p n ma mb (Just o) = do
  let delegationsTable = "s1"
      opsTable = "s2"
      delegationsFields = strFieldNamesInScope (undefined :: OpDelegations) delegationsTable
      opsFields = strFieldNamesInScope (undefined :: Ops) opsTable
      sn = safeNumberPerPage n
      pn = PersistInt64 $ fromIntegral sn
      po = PersistInt64 $ fromIntegral $ offset p sn
      (blockSql, blockParamSql, blockCheck) = checkBlock mb (Just delegationsTable)
      (accSql, accParamSql, accCheck) = checkDelegationAcc ma
      (opParamSql, opCheck) = checkOp' o
      query = unwords
                [ " select " <> opsFields <> "," <> delegationsFields
                , " from "
                  , "(select * from ops where ops.op_hash = ?) as " <> opsTable
                , " inner join op_delegations " <> delegationsTable
                , " on " <> opsTable <> ".uuid = " <> delegationsTable <> ".op_uuid"
                , " where " <> delegationsTable <> ".block_level is not null"
                  , " " <> blockSql
                  , " " <> accSql
                , " order by " <> delegationsTable <> ".block_level desc, " <> delegationsTable <> ".uuid asc"
                , " limit ? offset ?"
                ]
  if accCheck && opCheck && blockCheck then
    liftIO $ flip G.runDbConn pool
           $ raw_ query (opParamSql <> blockParamSql <> accParamSql <> [ pn, po ])
           $ \xs -> do
                    (v1, r1) <- fromEntity xs
                    (v2, _) <- fromEntity r1
                    return $ zipWith D v1 v2
  else
    return []

delegationsNum :: Pool Postgresql -> Maybe String -> Maybe String -> Maybe String -> IO Int
delegationsNum pool ma mb Nothing = do
  let (blockSql, blockParamSql, blockCheck) = checkBlock mb Nothing
      (accSql, accParamSql, accCheck) = checkDelegationAcc ma
      query = unwords
                [ " select count(*) from op_delegations"
                , " where block_level is not null"
                  , " " <> blockSql
                  , " " <> accSql
                ]
  if accCheck && blockCheck then
    liftIO $ flip G.runDbConn pool $ raw_ query (blockParamSql <> accParamSql)
           $ \xs -> fromSinglePersistValue $ head $ head xs
  else
    return 0
delegationsNum pool ma mb (Just o) = do
  let delegationsTable = "s1"
      opsTable = "s2"
      (blockSql, blockParamSql, blockCheck) = checkBlock mb (Just delegationsTable)
      (accSql, accParamSql, accCheck) = checkDelegationAcc ma
      (opParamSql, opCheck) = checkOp' o
      query = unwords
                [ " select count(*)"
                , " from "
                  , "(select * from ops where ops.op_hash = ?) as " <> opsTable
                , " inner join op_delegations " <> delegationsTable
                , " on " <> opsTable <> ".uuid = " <> delegationsTable <> ".op_uuid"
                , " where " <> delegationsTable <> ".block_level is not null"
                  , " " <> blockSql
                  , " " <> accSql
                ]
  if accCheck && blockCheck && opCheck then
    liftIO $ flip G.runDbConn pool
           $ raw_ query (opParamSql <> blockParamSql <> accParamSql)
           $ \xs -> fromSinglePersistValue $ head $ head xs
  else
    return 0

--- Origination

lastesOriginations :: Pool Postgresql
           -> Int      -- ^ Page
           -> Int      -- ^ Number
           -> Maybe String -- ^ account
           -> Maybe String -- ^ block
           -> Maybe String -- ^ op
           -> IO [ OpOriginationsDetail ]
lastesOriginations pool p n ma mb Nothing = do
  let originationsTable = "s1"
      opsTable = "s2"
      originationsFields = strFieldNamesInScope (undefined :: OpOriginations) originationsTable
      opsFields = strFieldNamesInScope (undefined :: Ops) opsTable
      sn = safeNumberPerPage n
      pn = PersistInt64 $ fromIntegral sn
      po = PersistInt64 $ fromIntegral $ offset p sn
      (blockSql, blockParamSql, blockCheck) = checkBlock mb Nothing
      (accSql, accParamSql, accCheck) = checkOriginationAcc ma
      query = unwords
                [ " select " <> opsFields <> "," <> originationsFields
                , " from "
                  , " (select " <> originationsFields <> " from op_originations as " <> originationsTable
                    , " where block_level is not null"
                    , " " <> blockSql
                    , " " <> accSql
                    , " order by block_level desc, uuid asc"
                    , " limit ? offset ?) as " <> originationsTable
                , " left join ops " <> opsTable
                , " on " <> opsTable <> ".uuid = " <> originationsTable <> ".op_uuid"
                ]
  if accCheck && blockCheck then
    liftIO $ flip G.runDbConn pool
           $ raw_ query (blockParamSql <> accParamSql <> [ pn, po ])
           $ \xs -> do
               (v1, r1) <- fromEntity xs
               (v2, _) <- fromEntity r1
               return $ zipWith O v1 v2
  else
    return []
lastesOriginations pool p n ma mb (Just o) = do
  let originationsTable = "s1"
      opsTable = "s2"
      originationsFields = strFieldNamesInScope (undefined :: OpOriginations) originationsTable
      opsFields = strFieldNamesInScope (undefined :: Ops) opsTable
      sn = safeNumberPerPage n
      pn = PersistInt64 $ fromIntegral sn
      po = PersistInt64 $ fromIntegral $ offset p sn
      (blockSql, blockParamSql, blockCheck) = checkBlock mb (Just originationsTable)
      (accSql, accParamSql, accCheck) = checkOriginationAcc ma
      (opParamSql, opCheck) = checkOp' o
      query = unwords
                [ " select " <> opsFields <> "," <> originationsFields
                , " from "
                  , "(select * from ops where ops.op_hash = ?) as " <> opsTable
                , " inner join op_originations " <> originationsTable
                , " on " <> opsTable <> ".uuid = " <> originationsTable <> ".op_uuid"
                , " where " <> originationsTable <> ".block_level is not null"
                  , " " <> blockSql
                  , " " <> accSql
                , " order by " <> originationsTable <> ".block_level desc, " <> originationsTable <> ".uuid asc"
                , " limit ? offset ?"
                ]
  if accCheck && opCheck && blockCheck then
    liftIO $ flip G.runDbConn pool
           $ raw_ query (opParamSql <> blockParamSql <> accParamSql <> [ pn, po ])
           $ \xs -> do
                    (v1, r1) <- fromEntity xs
                    (v2, _) <- fromEntity r1
                    return $ zipWith O v1 v2
  else
    return []

originationsNum :: Pool Postgresql -> Maybe String -> Maybe String -> Maybe String -> IO Int
originationsNum pool ma mb Nothing = do
  let (blockSql, blockParamSql, blockCheck) = checkBlock mb Nothing
      (accSql, accParamSql, accCheck) = checkOriginationAcc ma
      query = unwords
                [ " select count(*) from op_originations"
                , " where block_level is not null"
                  , " " <> blockSql
                  , " " <> accSql
                ]
  if accCheck && blockCheck then
    liftIO $ flip G.runDbConn pool $ raw_ query (blockParamSql <> accParamSql)
           $ \xs -> fromSinglePersistValue $ head $ head xs
  else
    return 0
originationsNum pool ma mb (Just o) = do
  let originationsTable = "s1"
      opsTable = "s2"
      (blockSql, blockParamSql, blockCheck) = checkBlock mb (Just originationsTable)
      (accSql, accParamSql, accCheck) = checkOriginationAcc ma
      (opParamSql, opCheck) = checkOp' o
      query = unwords
                [ " select count(*)"
                , " from "
                  , "(select * from ops where ops.op_hash = ?) as " <> opsTable
                , " inner join op_originations " <> originationsTable
                , " on " <> opsTable <> ".uuid = " <> originationsTable <> ".op_uuid"
                , " where " <> originationsTable <> ".block_level is not null"
                  , " " <> blockSql
                  , " " <> accSql
                ]
  if accCheck && blockCheck && opCheck then
    liftIO $ flip G.runDbConn pool
           $ raw_ query (opParamSql <> blockParamSql <> accParamSql)
           $ \xs -> fromSinglePersistValue $ head $ head xs
  else
    return 0

--- Ballot

lastesBallots :: Pool Postgresql
           -> Int      -- ^ Page
           -> Int      -- ^ Number
           -> Maybe String -- ^ account
           -> Maybe String -- ^ block
           -> Maybe String -- ^ op
           -> Maybe String -- ^ proposal
           -> IO [ OpBallotsDetail ]
lastesBallots pool p n ma mb Nothing mp = do
  let ballotsTable = "s1"
      opsTable = "s2"
      ballotsFields = strFieldNamesInScope (undefined :: OpBallots) ballotsTable
      opsFields = strFieldNamesInScope (undefined :: Ops) opsTable
      sn = safeNumberPerPage n
      pn = PersistInt64 $ fromIntegral sn
      po = PersistInt64 $ fromIntegral $ offset p sn
      (blockSql, blockParamSql, blockCheck) = checkBlock mb Nothing
      (accSql, accParamSql, accCheck) = checkBallotAcc ma
      (ppSSql, ppSParamSql, ppSCheck) = checkProposalPps mp
      query = unwords
                [ " select " <> opsFields <> "," <> ballotsFields
                , " from "
                  , " (select " <> ballotsFields <> " from op_ballots as " <> ballotsTable
                    , " where block_level is not null"
                    , " " <> blockSql
                    , " " <> accSql
                    , " " <> ppSSql
                    , " order by block_level desc, uuid asc"
                    , " limit ? offset ?) as " <> ballotsTable
                , " left join ops " <> opsTable
                , " on " <> opsTable <> ".uuid = " <> ballotsTable <> ".op_uuid"
                ]
  if accCheck && blockCheck && ppSCheck then
    liftIO $ flip G.runDbConn pool
           $ raw_ query (blockParamSql <> accParamSql <> ppSParamSql <> [ pn, po ])
           $ \xs -> do
               (v1, r1) <- fromEntity xs
               (v2, _) <- fromEntity r1
               return $ zipWith B v1 v2
  else
    return []
lastesBallots pool p n ma mb (Just o) mp = do
  let ballotsTable = "s1"
      opsTable = "s2"
      ballotsFields = strFieldNamesInScope (undefined :: OpBallots) ballotsTable
      opsFields = strFieldNamesInScope (undefined :: Ops) opsTable
      sn = safeNumberPerPage n
      pn = PersistInt64 $ fromIntegral sn
      po = PersistInt64 $ fromIntegral $ offset p sn
      (blockSql, blockParamSql, blockCheck) = checkBlock mb (Just ballotsTable)
      (accSql, accParamSql, accCheck) = checkBallotAcc ma
      (opParamSql, opCheck) = checkOp' o
      (ppSSql, ppSParamSql, ppSCheck) = checkProposalPps mp
      query = unwords
                [ " select " <> opsFields <> "," <> ballotsFields
                , " from "
                  , "(select * from ops where ops.op_hash = ?) as " <> opsTable
                , " inner join op_ballots " <> ballotsTable
                , " on " <> opsTable <> ".uuid = " <> ballotsTable <> ".op_uuid"
                , " where " <> ballotsTable <> ".block_level is not null"
                  , " " <> blockSql
                  , " " <> accSql
                  , " " <> ppSSql
                , " order by " <> ballotsTable <> ".block_level desc, " <> ballotsTable <> ".uuid asc"
                , " limit ? offset ?"
                ]
  if accCheck && opCheck && blockCheck && ppSCheck then
    liftIO $ flip G.runDbConn pool
           $ raw_ query (opParamSql <> blockParamSql <> accParamSql <> ppSParamSql <> [ pn, po ])
           $ \xs -> do
                    (v1, r1) <- fromEntity xs
                    (v2, _) <- fromEntity r1
                    return $ zipWith B v1 v2
  else
    return []

ballotsNum :: Pool Postgresql -> Maybe String -> Maybe String -> Maybe String -> Maybe String -> IO Int
ballotsNum pool ma mb Nothing mp = do
  let (blockSql, blockParamSql, blockCheck) = checkBlock mb Nothing
      (accSql, accParamSql, accCheck) = checkBallotAcc ma
      (ppSSql, ppSParamSql, ppSCheck) = checkProposalPps mp
      query = unwords
                [ " select count(*) from op_ballots"
                , " where block_level is not null"
                  , " " <> blockSql
                  , " " <> accSql
                  , " " <> ppSSql
                ]
  if accCheck && blockCheck && ppSCheck then
    liftIO $ flip G.runDbConn pool $ raw_ query (blockParamSql <> accParamSql <> ppSParamSql)
           $ \xs -> fromSinglePersistValue $ head $ head xs
  else
    return 0
ballotsNum pool ma mb (Just o) mp = do
  let ballotsTable = "s1"
      opsTable = "s2"
      (blockSql, blockParamSql, blockCheck) = checkBlock mb (Just ballotsTable)
      (accSql, accParamSql, accCheck) = checkBallotAcc ma
      (opParamSql, opCheck) = checkOp' o
      (ppSSql, ppSParamSql, ppSCheck) = checkProposalPps mp
      query = unwords
                [ " select count(*)"
                , " from "
                  , "(select * from ops where ops.op_hash = ?) as " <> opsTable
                , " inner join op_ballots " <> ballotsTable
                , " on " <> opsTable <> ".uuid = " <> ballotsTable <> ".op_uuid"
                , " where " <> ballotsTable <> ".block_level is not null"
                  , " " <> blockSql
                  , " " <> accSql
                  , " " <> ppSSql
                ]
  if accCheck && blockCheck && opCheck && ppSCheck then
    liftIO $ flip G.runDbConn pool
           $ raw_ query (opParamSql <> blockParamSql <> accParamSql <> ppSParamSql)
           $ \xs -> fromSinglePersistValue $ head $ head xs
  else
    return 0

--- DoubleEndorsementEvidence

lastesDoubleEndorsementEvidences :: Pool Postgresql
           -> Int      -- ^ Page
           -> Int      -- ^ Number
           -> Maybe String -- ^ block
           -> Maybe String -- ^ op
           -> IO [ OpDoubleEndorsementEvidencesDetail ]
lastesDoubleEndorsementEvidences pool p n mb Nothing = do
  let doubleEndorsementEvidencesTable = "s1"
      opsTable = "s2"
      op1sFields = strFieldNamesInScope (undefined :: Op1s) "op1s"
      op2sFields = strFieldNamesInScope (undefined :: Op2s) "op2s"
      doubleEndorsementEvidencesFields = strFieldNamesInScope (undefined :: OpDoubleEndorsementEvidences ) doubleEndorsementEvidencesTable
      opsFields = strFieldNamesInScope (undefined :: Ops) opsTable
      sn = safeNumberPerPage n
      pn = PersistInt64 $ fromIntegral sn
      po = PersistInt64 $ fromIntegral $ offset p sn
      (blockSql, blockParamSql, blockCheck) = checkBlock mb Nothing
      query = unwords
                [ " select " <> opsFields <> "," <> doubleEndorsementEvidencesFields
                  , ", " <> op1sFields <> ", " <> op2sFields
                , " from "
                  , " (select " <> doubleEndorsementEvidencesFields <> " from op_double_endorsement_evidences as " <> doubleEndorsementEvidencesTable
                    , " where block_level is not null"
                    , " " <> blockSql
                    , " order by block_level desc, uuid asc"
                    , " limit ? offset ?) as " <> doubleEndorsementEvidencesTable
                , " left join ops " <> opsTable
                , " on " <> opsTable <> ".uuid = " <> doubleEndorsementEvidencesTable <> ".op_uuid"
                , " left join op1s "
                , " on " <> doubleEndorsementEvidencesTable <> ".uuid = op1s.double_endorsement_evidences_uuid"
                , " left join op2s "
                , " on " <> doubleEndorsementEvidencesTable <> ".uuid = op2s.double_endorsement_evidences_uuid"
                ]
  if blockCheck then
    liftIO $ flip G.runDbConn pool
           $ raw_ query (blockParamSql <> [ pn, po ])
           $ \xs -> do
               (v1, r1) <- fromEntity xs
               (v2, r2) <- fromEntity r1
               (v3, r3) <- fromEntity r2
               (v4, _) <- fromEntity r3
               let uncurry4 = $(uncurryN 4)
               return $ map (uncurry4 DEE) $ zip4 v1 v2 v3 v4
  else
    return []
lastesDoubleEndorsementEvidences pool p n mb (Just o) = do
  let doubleEndorsementEvidencesTable = "s1"
      opsTable = "s2"
      doubleEndorsementEvidencesFields = strFieldNamesInScope (undefined :: OpDoubleEndorsementEvidences) doubleEndorsementEvidencesTable
      opsFields = strFieldNamesInScope (undefined :: Ops) opsTable
      op1sFields = strFieldNamesInScope (undefined :: Op1s) "op1s"
      op2sFields = strFieldNamesInScope (undefined :: Op2s) "op2s"
      sn = safeNumberPerPage n
      pn = PersistInt64 $ fromIntegral sn
      po = PersistInt64 $ fromIntegral $ offset p sn
      (blockSql, blockParamSql, blockCheck) = checkBlock mb (Just doubleEndorsementEvidencesTable)
      (opParamSql, opCheck) = checkOp' o
      query = unwords
                [ " select " <> opsFields <> "," <> doubleEndorsementEvidencesFields
                  , ", " <> op1sFields <> ", " <> op2sFields
                , " from "
                  , "(select * from ops where ops.op_hash = ?) as " <> opsTable
                , " inner join op_double_endorsement_evidences " <> doubleEndorsementEvidencesTable
                , " on " <> opsTable <> ".uuid = " <> doubleEndorsementEvidencesTable <> ".op_uuid"
                  , " " <> blockSql
                , " left join op1s "
                , " on " <> doubleEndorsementEvidencesTable <> ".uuid = op1s.double_endorsement_evidences_uuid"
                , " left join op2s "
                , " on " <> doubleEndorsementEvidencesTable <> ".uuid = op2s.double_endorsement_evidences_uuid"
                , " where " <> doubleEndorsementEvidencesTable <> ".block_level is not null"
                , " order by " <> doubleEndorsementEvidencesTable <> ".block_level desc, " <> doubleEndorsementEvidencesTable <> ".uuid asc"
                , " limit ? offset ?"
                ]
  if opCheck && blockCheck then
    liftIO $ flip G.runDbConn pool
           $ raw_ query (opParamSql <> blockParamSql <> [ pn, po ])
           $ \xs -> do
                    (v1, r1) <- fromEntity xs
                    (v2, r2) <- fromEntity r1
                    (v3, r3) <- fromEntity r2
                    (v4, _) <- fromEntity r3
                    let uncurry4 = $(uncurryN 4)
                    return $ map (uncurry4 DEE) $ zip4 v1 v2 v3 v4
  else
    return []

doubleEndorsementEvidencesNum :: Pool Postgresql -> Maybe String -> Maybe String -> IO Int
doubleEndorsementEvidencesNum pool mb Nothing = do
  let (blockSql, blockParamSql, blockCheck) = checkBlock mb Nothing
      query = unwords
                [ " select count(*) from op_double_endorsement_evidences"
                , " where block_level is not null"
                  , " " <> blockSql
                ]
  if blockCheck then
    liftIO $ flip G.runDbConn pool $ raw_ query blockParamSql
           $ \xs -> fromSinglePersistValue $ head $ head xs
  else
    return 0
doubleEndorsementEvidencesNum pool mb (Just o) = do
  let doubleEndorsementEvidencesTable = "s1"
      opsTable = "s2"
      (blockSql, blockParamSql, blockCheck) = checkBlock mb (Just doubleEndorsementEvidencesTable)
      (opParamSql, opCheck) = checkOp' o
      query = unwords
                [ " select count(*)"
                , " from "
                  , "(select * from ops where ops.op_hash = ?) as " <> opsTable
                , " inner join op_double_endorsement_evidences " <> doubleEndorsementEvidencesTable
                , " on " <> opsTable <> ".uuid = " <> doubleEndorsementEvidencesTable <> ".op_uuid"
                , " where " <> doubleEndorsementEvidencesTable <> ".block_level is not null"
                  , " " <> blockSql
                ]
  if blockCheck && opCheck then
    liftIO $ flip G.runDbConn pool
           $ raw_ query (opParamSql <> blockParamSql)
           $ \xs -> fromSinglePersistValue $ head $ head xs
  else
    return 0

--- DoubleBakingEvidence

lastesDoubleBakingEvidences :: Pool Postgresql
           -> Int      -- ^ Page
           -> Int      -- ^ Number
           -> Maybe String -- ^ block
           -> Maybe String -- ^ op
           -> IO [ OpDoubleBakingEvidencesDetail ]
lastesDoubleBakingEvidences pool p n mb Nothing = do
  let doubleBakingEvidencesTable = "s1"
      opsTable = "s2"
      bh1sFields = strFieldNamesInScope (undefined :: Bh1s) "bh1s"
      bh2sFields = strFieldNamesInScope (undefined :: Bh2s) "bh2s"
      doubleBakingEvidencesFields = strFieldNamesInScope (undefined :: OpDoubleBakingEvidences ) doubleBakingEvidencesTable
      opsFields = strFieldNamesInScope (undefined :: Ops) opsTable
      sn = safeNumberPerPage n
      pn = PersistInt64 $ fromIntegral sn
      po = PersistInt64 $ fromIntegral $ offset p sn
      (blockSql, blockParamSql, blockCheck) = checkBlock mb Nothing
      query = unwords
                [ " select " <> opsFields <> "," <> doubleBakingEvidencesFields
                  , ", " <> bh1sFields <> ", " <> bh2sFields
                , " from "
                  , " (select " <> doubleBakingEvidencesFields <> " from op_double_baking_evidences as " <> doubleBakingEvidencesTable
                    , " where block_level is not null"
                    , " " <> blockSql
                    , " order by block_level desc, uuid asc"
                    , " limit ? offset ?) as " <> doubleBakingEvidencesTable
                , " left join ops " <> opsTable
                , " on " <> opsTable <> ".uuid = " <> doubleBakingEvidencesTable <> ".op_uuid"
                , " left join bh1s "
                , " on " <> doubleBakingEvidencesTable <> ".uuid = bh1s.double_baking_evidences_uuid"
                , " left join bh2s "
                , " on " <> doubleBakingEvidencesTable <> ".uuid = bh2s.double_baking_evidences_uuid"
                , " order by " <> doubleBakingEvidencesTable <> ".block_level desc"
                ]
  if blockCheck then
    liftIO $ flip G.runDbConn pool
           $ raw_ query (blockParamSql <> [ pn, po ])
           $ \xs -> do
               (v1, r1) <- fromEntity xs
               (v2, r2) <- fromEntity r1
               (v3, r3) <- fromEntity r2
               (v4, _) <- fromEntity r3
               let uncurry4 = $(uncurryN 4)
               return $ map (uncurry4 DBE) $ zip4 v1 v2 v3 v4
  else
    return []
lastesDoubleBakingEvidences pool p n mb (Just o) = do
  let doubleBakingEvidencesTable = "s1"
      opsTable = "s2"
      doubleBakingEvidencesFields = strFieldNamesInScope (undefined :: OpDoubleBakingEvidences) doubleBakingEvidencesTable
      opsFields = strFieldNamesInScope (undefined :: Ops) opsTable
      bh1sFields = strFieldNamesInScope (undefined :: Bh1s) "bh1s"
      bh2sFields = strFieldNamesInScope (undefined :: Bh2s) "bh2s"
      sn = safeNumberPerPage n
      pn = PersistInt64 $ fromIntegral sn
      po = PersistInt64 $ fromIntegral $ offset p sn
      (blockSql, blockParamSql, blockCheck) = checkBlock mb (Just doubleBakingEvidencesTable)
      (opParamSql, opCheck) = checkOp' o
      query = unwords
                [ " select " <> opsFields <> "," <> doubleBakingEvidencesFields
                  , ", " <> bh1sFields <> ", " <> bh2sFields
                , " from "
                  , "(select * from ops where ops.op_hash = ?) as " <> opsTable
                , " inner join op_double_baking_evidences " <> doubleBakingEvidencesTable
                , " on " <> opsTable <> ".uuid = " <> doubleBakingEvidencesTable <> ".op_uuid"
                  , " " <> blockSql
                , " left join bh1s "
                , " on " <> doubleBakingEvidencesTable <> ".uuid = bh1s.double_baking_evidences_uuid"
                , " left join bh2s "
                , " on " <> doubleBakingEvidencesTable <> ".uuid = bh2s.double_baking_evidences_uuid"
                , " where " <> doubleBakingEvidencesTable <> ".block_level is not null"
                , " order by " <> doubleBakingEvidencesTable <> ".block_level desc, " <> doubleBakingEvidencesTable <> ".uuid asc"
                , " limit ? offset ?"
                ]
  if opCheck && blockCheck then
    liftIO $ flip G.runDbConn pool
           $ raw_ query (opParamSql <> blockParamSql <> [ pn, po ])
           $ \xs -> do
                    (v1, r1) <- fromEntity xs
                    (v2, r2) <- fromEntity r1
                    (v3, r3) <- fromEntity r2
                    (v4, _) <- fromEntity r3
                    let uncurry4 = $(uncurryN 4)
                    return $ map (uncurry4 DBE) $ zip4 v1 v2 v3 v4
  else
    return []

doubleBakingEvidencesNum :: Pool Postgresql -> Maybe String -> Maybe String -> IO Int
doubleBakingEvidencesNum pool mb Nothing = do
  let (blockSql, blockParamSql, blockCheck) = checkBlock mb Nothing
      query = unwords
                [ " select count(*) from op_double_baking_evidences"
                , " where block_level is not null"
                  , " " <> blockSql
                ]
  if blockCheck then
    liftIO $ flip G.runDbConn pool $ raw_ query blockParamSql
           $ \xs -> fromSinglePersistValue $ head $ head xs
  else
    return 0
doubleBakingEvidencesNum pool mb (Just o) = do
  let doubleBakingEvidencesTable = "s1"
      opsTable = "s2"
      (blockSql, blockParamSql, blockCheck) = checkBlock mb (Just doubleBakingEvidencesTable)
      (opParamSql, opCheck) = checkOp' o
      query = unwords
                [ " select count(*)"
                , " from "
                  , "(select * from ops where ops.op_hash = ?) as " <> opsTable
                , " inner join op_double_baking_evidences " <> doubleBakingEvidencesTable
                , " on " <> opsTable <> ".uuid = " <> doubleBakingEvidencesTable <> ".op_uuid"
                , " where " <> doubleBakingEvidencesTable <> ".block_level is not null"
                  , " " <> blockSql
                ]
  if blockCheck && opCheck then
    liftIO $ flip G.runDbConn pool
           $ raw_ query (opParamSql <> blockParamSql)
           $ \xs -> fromSinglePersistValue $ head $ head xs
  else
    return 0

--- SeedNonceEvelation

lastesSeedNonceEvelations :: Pool Postgresql
           -> Int      -- ^ Page
           -> Int      -- ^ Number
           -> Maybe String -- ^ block
           -> Maybe String -- ^ op
           -> IO [ OpSeedNonceRevelationsDetail ]
lastesSeedNonceEvelations pool p n mb Nothing = do
  let seedNonceEvelationsTable = "s1"
      opsTable = "s2"
      seedNonceEvelationsFields = strFieldNamesInScope (undefined :: OpSeedNonceRevelations) seedNonceEvelationsTable
      opsFields = strFieldNamesInScope (undefined :: Ops) opsTable
      sn = safeNumberPerPage n
      pn = PersistInt64 $ fromIntegral sn
      po = PersistInt64 $ fromIntegral $ offset p sn
      (blockSql, blockParamSql, blockCheck) = checkBlock mb Nothing
      query = unwords
                [ " select " <> opsFields <> "," <> seedNonceEvelationsFields
                , " from "
                  , " (select " <> seedNonceEvelationsFields <> " from op_seed_nonce_revelations as " <> seedNonceEvelationsTable
                    , " where block_level is not null"
                    , " " <> blockSql
                    , " order by block_level desc, uuid asc"
                    , " limit ? offset ?) as " <> seedNonceEvelationsTable
                , " left join ops " <> opsTable
                , " on " <> opsTable <> ".uuid = " <> seedNonceEvelationsTable <> ".op_uuid"
                ]
  if blockCheck then
    liftIO $ flip G.runDbConn pool
           $ raw_ query (blockParamSql <> [ pn, po ])
           $ \xs -> do
               (v1, r1) <- fromEntity xs
               (v2, _) <- fromEntity r1
               return $ zipWith S v1 v2
  else
    return []
lastesSeedNonceEvelations pool p n mb (Just o) = do
  let seedNonceEvelationsTable = "s1"
      opsTable = "s2"
      seedNonceEvelationsFields = strFieldNamesInScope (undefined :: OpSeedNonceRevelations) seedNonceEvelationsTable
      opsFields = strFieldNamesInScope (undefined :: Ops) opsTable
      sn = safeNumberPerPage n
      pn = PersistInt64 $ fromIntegral sn
      po = PersistInt64 $ fromIntegral $ offset p sn
      (blockSql, blockParamSql, blockCheck) = checkBlock mb (Just seedNonceEvelationsTable)
      (opParamSql, opCheck) = checkOp' o
      query = unwords
                [ " select " <> opsFields <> "," <> seedNonceEvelationsFields
                , " from "
                  , "(select * from ops where ops.op_hash = ?) as " <> opsTable
                , " inner join op_seed_nonce_revelations " <> seedNonceEvelationsTable
                , " on " <> opsTable <> ".uuid = " <> seedNonceEvelationsTable <> ".op_uuid"
                , " where " <> seedNonceEvelationsTable <> ".block_level is not null"
                  , " " <> blockSql
                , " order by " <> seedNonceEvelationsTable <> ".block_level desc, " <> seedNonceEvelationsTable <> ".uuid asc"
                , " limit ? offset ?"
                ]
  if opCheck && blockCheck then
    liftIO $ flip G.runDbConn pool
           $ raw_ query (opParamSql <> blockParamSql <> [ pn, po ])
           $ \xs -> do
                    (v1, r1) <- fromEntity xs
                    (v2, _) <- fromEntity r1
                    return $ zipWith S v1 v2
  else
    return []

seedNonceEvelationsNum :: Pool Postgresql -> Maybe String -> Maybe String -> IO Int
seedNonceEvelationsNum pool mb Nothing = do
  let (blockSql, blockParamSql, blockCheck) = checkBlock mb Nothing
      query = unwords
                [ " select count(*) from op_seed_nonce_revelations"
                , " where block_level is not null"
                  , " " <> blockSql
                ]
  if blockCheck then
    liftIO $ flip G.runDbConn pool $ raw_ query blockParamSql
           $ \xs -> fromSinglePersistValue $ head $ head xs
  else
    return 0
seedNonceEvelationsNum pool mb (Just o) = do
  let seedNonceEvelationsTable = "s1"
      opsTable = "s2"
      (blockSql, blockParamSql, blockCheck) = checkBlock mb (Just seedNonceEvelationsTable)
      (opParamSql, opCheck) = checkOp' o
      query = unwords
                [ " select count(*)"
                , " from "
                  , "(select * from ops where ops.op_hash = ?) as " <> opsTable
                , " inner join op_seed_nonce_revelations " <> seedNonceEvelationsTable
                , " on " <> opsTable <> ".uuid = " <> seedNonceEvelationsTable <> ".op_uuid"
                , " where " <> seedNonceEvelationsTable <> ".block_level is not null"
                  , " " <> blockSql
                ]
  if blockCheck && opCheck then
    liftIO $ flip G.runDbConn pool
           $ raw_ query (opParamSql <> blockParamSql)
           $ \xs -> fromSinglePersistValue $ head $ head xs
  else
    return 0

--- Reveal

lastesReveals :: Pool Postgresql
           -> Int      -- ^ Page
           -> Int      -- ^ Number
           -> Maybe String -- ^ account
           -> Maybe String -- ^ block
           -> Maybe String -- ^ op
           -> IO [ OpRevealsDetail ]
lastesReveals pool p n ma mb Nothing = do
  let revealsTable = "s1"
      opsTable = "s2"
      revealsFields = strFieldNamesInScope (undefined :: OpReveals) revealsTable
      opsFields = strFieldNamesInScope (undefined :: Ops) opsTable
      sn = safeNumberPerPage n
      pn = PersistInt64 $ fromIntegral sn
      po = PersistInt64 $ fromIntegral $ offset p sn
      (blockSql, blockParamSql, blockCheck) = checkBlock mb Nothing
      (accSql, accParamSql, accCheck) = checkRevealAcc ma
      query = unwords
                [ " select " <> opsFields <> "," <> revealsFields
                , " from "
                  , " (select " <> revealsFields <> " from op_reveals as " <> revealsTable
                    , " where block_level is not null"
                    , " " <> blockSql
                    , " " <> accSql
                    , " order by block_level desc, uuid asc"
                    , " limit ? offset ?) as " <> revealsTable
                , " left join ops " <> opsTable
                , " on " <> opsTable <> ".uuid = " <> revealsTable <> ".op_uuid"
                ]
  if accCheck && blockCheck then
    liftIO $ flip G.runDbConn pool
           $ raw_ query (blockParamSql <> accParamSql <> [ pn, po ])
           $ \xs -> do
               (v1, r1) <- fromEntity xs
               (v2, _) <- fromEntity r1
               return $ zipWith R v1 v2
  else
    return []
lastesReveals pool p n ma mb (Just o) = do
  let revealsTable = "s1"
      opsTable = "s2"
      revealsFields = strFieldNamesInScope (undefined :: OpReveals) revealsTable
      opsFields = strFieldNamesInScope (undefined :: Ops) opsTable
      sn = safeNumberPerPage n
      pn = PersistInt64 $ fromIntegral sn
      po = PersistInt64 $ fromIntegral $ offset p sn
      (blockSql, blockParamSql, blockCheck) = checkBlock mb (Just revealsTable)
      (accSql, accParamSql, accCheck) = checkRevealAcc ma
      (opParamSql, opCheck) = checkOp' o
      query = unwords
                [ " select " <> opsFields <> "," <> revealsFields
                , " from "
                  , "(select * from ops where ops.op_hash = ?) as " <> opsTable
                , " inner join op_reveals " <> revealsTable
                , " on " <> opsTable <> ".uuid = " <> revealsTable <> ".op_uuid"
                , " where " <> revealsTable <> ".block_level is not null"
                  , " " <> blockSql
                  , " " <> accSql
                , " order by " <> revealsTable <> ".block_level desc, " <> revealsTable <> ".uuid asc"
                , " limit ? offset ?"
                ]
  if accCheck && opCheck && blockCheck then
    liftIO $ flip G.runDbConn pool
           $ raw_ query (opParamSql <> blockParamSql <> accParamSql <> [ pn, po ])
           $ \xs -> do
                    (v1, r1) <- fromEntity xs
                    (v2, _) <- fromEntity r1
                    return $ zipWith R v1 v2
  else
    return []

revealsNum :: Pool Postgresql -> Maybe String -> Maybe String -> Maybe String -> IO Int
revealsNum pool ma mb Nothing = do
  let (blockSql, blockParamSql, blockCheck) = checkBlock mb Nothing
      (accSql, accParamSql, accCheck) = checkRevealAcc ma
      query = unwords
                [ " select count(*) from op_reveals"
                , " where block_level is not null"
                  , " " <> blockSql
                  , " " <> accSql
                ]
  if accCheck && blockCheck then
    liftIO $ flip G.runDbConn pool $ raw_ query (blockParamSql <> accParamSql)
           $ \xs -> fromSinglePersistValue $ head $ head xs
  else
    return 0
revealsNum pool ma mb (Just o) = do
  let revealsTable = "s1"
      opsTable = "s2"
      (blockSql, blockParamSql, blockCheck) = checkBlock mb (Just revealsTable)
      (accSql, accParamSql, accCheck) = checkRevealAcc ma
      (opParamSql, opCheck) = checkOp' o
      query = unwords
                [ " select count(*)"
                , " from "
                  , "(select * from ops where ops.op_hash = ?) as " <> opsTable
                , " inner join op_reveals " <> revealsTable
                , " on " <> opsTable <> ".uuid = " <> revealsTable <> ".op_uuid"
                , " where " <> revealsTable <> ".block_level is not null"
                  , " " <> blockSql
                  , " " <> accSql
                ]
  if accCheck && blockCheck && opCheck then
    liftIO $ flip G.runDbConn pool
           $ raw_ query (opParamSql <> blockParamSql <> accParamSql)
           $ \xs -> fromSinglePersistValue $ head $ head xs
  else
    return 0
--- ActivateAccount

lastesActivateAccounts :: Pool Postgresql
           -> Int      -- ^ Page
           -> Int      -- ^ Number
           -> Maybe String -- ^ account
           -> Maybe String -- ^ block
           -> Maybe String -- ^ op
           -> IO [ OpActivateAccountsDetail ]
lastesActivateAccounts pool p n ma mb Nothing = do
  let activateAccountsTable = "s1"
      opsTable = "s2"
      activateAccountsFields = strFieldNamesInScope (undefined :: OpActivateAccounts) activateAccountsTable
      opsFields = strFieldNamesInScope (undefined :: Ops) opsTable
      sn = safeNumberPerPage n
      pn = PersistInt64 $ fromIntegral sn
      po = PersistInt64 $ fromIntegral $ offset p sn
      (blockSql, blockParamSql, blockCheck) = checkBlock mb Nothing
      (accSql, accParamSql, accCheck) = checkActivateAccountAcc ma
      query = unwords
                [ " select " <> opsFields <> "," <> activateAccountsFields
                , " from "
                  , " (select " <> activateAccountsFields <> " from op_activate_accounts as " <> activateAccountsTable
                    , " where block_level is not null"
                    , " " <> blockSql
                    , " " <> accSql
                    , " order by block_level desc, uuid asc"
                    , " limit ? offset ?) as " <> activateAccountsTable
                , " left join ops " <> opsTable
                , " on " <> opsTable <> ".uuid = " <> activateAccountsTable <> ".op_uuid"
                ]
  if accCheck && blockCheck then
    liftIO $ flip G.runDbConn pool
           $ raw_ query (blockParamSql <> accParamSql <> [ pn, po ])
           $ \xs -> do
               (v1, r1) <- fromEntity xs
               (v2, _) <- fromEntity r1
               return $ zipWith A v1 v2
  else
    return []
lastesActivateAccounts pool p n ma mb (Just o) = do
  let activateAccountsTable = "s1"
      opsTable = "s2"
      activateAccountsFields = strFieldNamesInScope (undefined :: OpActivateAccounts) activateAccountsTable
      opsFields = strFieldNamesInScope (undefined :: Ops) opsTable
      sn = safeNumberPerPage n
      pn = PersistInt64 $ fromIntegral sn
      po = PersistInt64 $ fromIntegral $ offset p sn
      (blockSql, blockParamSql, blockCheck) = checkBlock mb (Just activateAccountsTable)
      (accSql, accParamSql, accCheck) = checkActivateAccountAcc ma
      (opParamSql, opCheck) = checkOp' o
      query = unwords
                [ " select " <> opsFields <> "," <> activateAccountsFields
                , " from "
                  , "(select * from ops where ops.op_hash = ?) as " <> opsTable
                , " inner join op_activate_accounts " <> activateAccountsTable
                , " on " <> opsTable <> ".uuid = " <> activateAccountsTable <> ".op_uuid"
                , " where " <> activateAccountsTable <> ".block_level is not null"
                  , " " <> blockSql
                  , " " <> accSql
                , " order by " <> activateAccountsTable <> ".block_level desc, " <> activateAccountsTable <> ".uuid asc"
                , " limit ? offset ?"
                ]
  if accCheck && opCheck && blockCheck then
    liftIO $ flip G.runDbConn pool
           $ raw_ query (opParamSql <> blockParamSql <> accParamSql <> [ pn, po ])
           $ \xs -> do
                    (v1, r1) <- fromEntity xs
                    (v2, _) <- fromEntity r1
                    return $ zipWith A v1 v2
  else
    return []

activateAccountsNum :: Pool Postgresql -> Maybe String -> Maybe String -> Maybe String -> IO Int
activateAccountsNum pool ma mb Nothing = do
  let (blockSql, blockParamSql, blockCheck) = checkBlock mb Nothing
      (accSql, accParamSql, accCheck) = checkActivateAccountAcc ma
      query = unwords
                [ " select count(*) from op_activate_accounts"
                , " where block_level is not null"
                  , " " <> blockSql
                  , " " <> accSql
                ]
  if accCheck && blockCheck then
    liftIO $ flip G.runDbConn pool $ raw_ query (blockParamSql <> accParamSql)
           $ \xs -> fromSinglePersistValue $ head $ head xs
  else
    return 0
activateAccountsNum pool ma mb (Just o) = do
  let activateAccountsTable = "s1"
      opsTable = "s2"
      (blockSql, blockParamSql, blockCheck) = checkBlock mb (Just activateAccountsTable)
      (accSql, accParamSql, accCheck) = checkActivateAccountAcc ma
      (opParamSql, opCheck) = checkOp' o
      query = unwords
                [ " select count(*)"
                , " from "
                  , "(select * from ops where ops.op_hash = ?) as " <> opsTable
                , " inner join op_activate_accounts " <> activateAccountsTable
                , " on " <> opsTable <> ".uuid = " <> activateAccountsTable <> ".op_uuid"
                , " where " <> activateAccountsTable <> ".block_level is not null"
                  , " " <> blockSql
                  , " " <> accSql
                ]
  if accCheck && blockCheck && opCheck then
    liftIO $ flip G.runDbConn pool
           $ raw_ query (opParamSql <> blockParamSql <> accParamSql)
           $ \xs -> fromSinglePersistValue $ head $ head xs
  else
    return 0

--- Proposal

lastesProposals :: Pool Postgresql
           -> Int      -- ^ Page
           -> Int      -- ^ Number
           -> Maybe String -- ^ account
           -> Maybe String -- ^ block
           -> Maybe String -- ^ op
           -> IO [ OpProposalsDetail ]
lastesProposals pool p n ma mb Nothing = do
  let proposalsTable = "s1"
      opsTable = "s2"
      proposalsFields = strFieldNamesInScope (undefined :: OpProposals) proposalsTable
      opsFields = strFieldNamesInScope (undefined :: Ops) opsTable
      sn = safeNumberPerPage n
      pn = PersistInt64 $ fromIntegral sn
      po = PersistInt64 $ fromIntegral $ offset p sn
      (blockSql, blockParamSql, blockCheck) = checkBlock mb Nothing
      (accSql, accParamSql, accCheck) = checkProposalAcc ma
      query = unwords
                [ " select " <> opsFields <> "," <> proposalsFields
                , " from "
                  , " (select " <> proposalsFields <> " from op_proposals as " <> proposalsTable
                    , " where block_level is not null"
                    , " " <> blockSql
                    , " " <> accSql
                    , " order by block_level desc, uuid asc"
                    , " limit ? offset ?) as " <> proposalsTable
                , " left join ops " <> opsTable
                , " on " <> opsTable <> ".uuid = " <> proposalsTable <> ".op_uuid"
                ]
  if accCheck && blockCheck then
    liftIO $ flip G.runDbConn pool
           $ raw_ query (blockParamSql <> accParamSql <> [ pn, po ])
           $ \xs -> do
               (v1, r1) <- fromEntity xs
               (v2, _) <- fromEntity r1
               return $ zipWith P v1 v2
  else
    return []
lastesProposals pool p n ma mb (Just o) = do
  let proposalsTable = "s1"
      opsTable = "s2"
      proposalsFields = strFieldNamesInScope (undefined :: OpProposals) proposalsTable
      opsFields = strFieldNamesInScope (undefined :: Ops) opsTable
      sn = safeNumberPerPage n
      pn = PersistInt64 $ fromIntegral sn
      po = PersistInt64 $ fromIntegral $ offset p sn
      (blockSql, blockParamSql, blockCheck) = checkBlock mb (Just proposalsTable)
      (accSql, accParamSql, accCheck) = checkProposalAcc ma
      (opParamSql, opCheck) = checkOp' o
      query = unwords
                [ " select " <> opsFields <> "," <> proposalsFields
                , " from "
                  , "(select * from ops where ops.op_hash = ?) as " <> opsTable
                , " inner join op_proposals " <> proposalsTable
                , " on " <> opsTable <> ".uuid = " <> proposalsTable <> ".op_uuid"
                , " where " <> proposalsTable <> ".block_level is not null"
                  , " " <> blockSql
                  , " " <> accSql
                , " order by " <> proposalsTable <> ".block_level desc, " <> proposalsTable <> ".uuid asc"
                , " limit ? offset ?"
                ]
  if accCheck && opCheck && blockCheck then
    liftIO $ flip G.runDbConn pool
           $ raw_ query (opParamSql <> blockParamSql <> accParamSql <> [ pn, po ])
           $ \xs -> do
                    (v1, r1) <- fromEntity xs
                    (v2, _) <- fromEntity r1
                    return $ zipWith P v1 v2
  else
    return []

proposalsNum :: Pool Postgresql -> Maybe String -> Maybe String -> Maybe String -> IO Int
proposalsNum pool ma mb Nothing = do
  let (blockSql, blockParamSql, blockCheck) = checkBlock mb Nothing
      (accSql, accParamSql, accCheck) = checkProposalAcc ma
      query = unwords
                [ " select count(*) from op_proposals"
                , " where block_level is not null"
                  , " " <> blockSql
                  , " " <> accSql
                ]
  if accCheck && blockCheck then
    liftIO $ flip G.runDbConn pool $ raw_ query (blockParamSql <> accParamSql)
           $ \xs -> fromSinglePersistValue $ head $ head xs
  else
    return 0
proposalsNum pool ma mb (Just o) = do
  let proposalsTable = "s1"
      opsTable = "s2"
      (blockSql, blockParamSql, blockCheck) = checkBlock mb (Just proposalsTable)
      (accSql, accParamSql, accCheck) = checkProposalAcc ma
      (opParamSql, opCheck) = checkOp' o
      query = unwords
                [ " select count(*)"
                , " from "
                  , "(select * from ops where ops.op_hash = ?) as " <> opsTable
                , " inner join op_proposals " <> proposalsTable
                , " on " <> opsTable <> ".uuid = " <> proposalsTable <> ".op_uuid"
                , " where " <> proposalsTable <> ".block_level is not null"
                  , " " <> blockSql
                  , " " <> accSql
                ]
  if accCheck && blockCheck && opCheck then
    liftIO $ flip G.runDbConn pool
           $ raw_ query (opParamSql <> blockParamSql <> accParamSql)
           $ \xs -> fromSinglePersistValue $ head $ head xs
  else
    return 0

--- Proposer

lastesProposers :: Pool Postgresql
           -> Int      -- ^ Page
           -> Int      -- ^ Number
           -> IO [ Proposers ]
lastesProposers pool p n = do
  let sn = safeNumberPerPage n
      pn = PersistInt64 $ fromIntegral sn
      po = PersistInt64 $ fromIntegral $ offset p sn
      query = unwords
        [ " select s1.proposal, s1.count, s2.period, s2.source, s2.block_level, s2.block_hash, s2.block_timestamp"
        , " from (select proposal, count(*) as count, min(block_level) as block_level from"
          , " (select unnest(proposals) as proposal, block_level from op_proposals ) as p group by (proposal)) as s1"
        , " left join op_proposals s2 on s1.proposal =  any(s2.proposals)"
          , " and s1.block_level = s2.block_level order by s1.block_level desc limit ? offset ?"
        ]
  liftIO $ flip G.runDbConn pool
         $ raw_ query [ pn, po ]
         $ \xs -> do
             (v1, _) <- fromEntity xs
             return v1
proposersNum :: Pool Postgresql -> IO Int
proposersNum pool = do
  let query = unwords
        [ "select count(*) from"
        , " (select proposal from (select unnest(proposals) as proposal from op_proposals)"
        , " as p group by (proposal)) as s1"
        ]
  liftIO $ flip G.runDbConn pool
         $ raw_ query []
         $ \xs -> fromSinglePersistValue $ head $ head xs

--- Protocal

lastesProtocols :: Pool Postgresql
           -> Int      -- ^ Page
           -> Int      -- ^ Number
           -> IO [ Protocols ]
lastesProtocols pool p n = do
  let sn = safeNumberPerPage n
      pn = PersistInt64 $ fromIntegral sn
      po = PersistInt64 $ fromIntegral $ offset p sn
      query = unwords
        [ " select protocol, min(proto) as proto, min(level) as from, max(level) as to from blocks group by protocol order by \"to\" desc limit ? offset ?"
        ]
  liftIO $ flip G.runDbConn pool
         $ raw_ query [ pn, po ]
         $ \xs -> do
             (v1, _) <- fromEntity xs
             return v1
protocolsNum :: Pool Postgresql -> IO Int
protocolsNum pool = do
  let query = unwords
        [ " select count(*) from (select protocol from blocks group by protocol) as c"
        ]
  liftIO $ flip G.runDbConn pool
         $ raw_ query []
         $ \xs -> fromSinglePersistValue $ head $ head xs

--- 24 hours stat
twentyFourStat :: Pool Postgresql -> IO Stat
twentyFourStat pool = do
  let query = unwords
        [ " select (select count(*) from blocks where timestamp > ?),"
        , " (select count(*) from op_txs where block_timestamp > ?)"
        ]

  t <- getCurrentTime
  let time = addUTCTime (negate $ 60 * 60 * 24) t
  liftIO $ flip G.runDbConn pool
         $ raw_ query (replicate 2 $ PersistText $ T.pack $ iso8601 time)
         $ \xs -> do
                (v1, r1) <- fromEntity xs
                (v2, _) <- fromEntity r1
                return $ head $ zipWith Stat v1 v2

--- Account
accountLists :: Pool Postgresql -> Int -> Int -> Maybe String -> IO [Accounts]
accountLists pool p n pf = do
    let accTable = "a"
        accFields = strFieldNamesInScope (undefined :: Accounts) accTable
        sn = safeNumberPerPage n
        pn = PersistInt64 $ fromIntegral sn
        po = PersistInt64 $ fromIntegral $ offset p sn
        (accQuery, accParam) =
          case pf of
            Just t -> ("where account like ?", [PersistText $ T.pack $ t <> "%"])
            Nothing -> ("", [])

        query = unwords
          [ "select " <> accFields <> " from accounts as " <> accTable <> " "
          , " " <> accQuery
          , " order by " <> accTable <> "._inserted_timestamp asc"
          , " limit ? offset ?"
          ]
    liftIO $ flip G.runDbConn pool $ raw_ query ( accParam <> [ pn, po ])
           $ \xs -> do
                    (v, _) <- fromEntity xs
                    return v

accountsNum :: Pool Postgresql -> Maybe String -> IO Int
accountsNum pool pf = do
    let accTable = "a"
        (accQuery, accParam) =
          case pf of
            Just t -> ("where account like ?", [PersistText $ T.pack $ t <> "%"])
            Nothing -> ("", [])

        query = unwords
          [ "select count(*) from accounts as " <> accTable <> " "
          , " " <> accQuery
          ]
    liftIO $ flip G.runDbConn pool $ raw_ query accParam
           $ \xs -> fromSinglePersistValue $ head $ head xs

iso8601 :: UTCTime -> String
iso8601 t = take 19 (formatTime defaultTimeLocale "%FT%T%Q" t) ++ "Z"

addPrefix :: Maybe String -> String
addPrefix Nothing = ""
addPrefix (Just p) = p <> "."

checkBlock :: Maybe String -> Maybe String -> (String, [PersistValue], Bool)
checkBlock Nothing _ = ("", [], True)
checkBlock (Just b) p
  | Prelude.all isDigit b =
     (" and " <> addPrefix p <> "block_level=?", [PersistInt64 $ read b], True)
  |  "B" `isPrefixOf` b =
     (" and " <> addPrefix p <> "block_hash=?", [PersistText $ T.pack b], True)
  | otherwise = ("", [], False)

checkBallotAcc :: Maybe String -> (String, [PersistValue], Bool)
checkBallotAcc Nothing = ("", [], True)
checkBallotAcc (Just a)
  | "tz" `isPrefixOf` a || "KT" `isPrefixOf` a =
    (" and source=? ", [PersistText $ T.pack a], True)
  | otherwise = ("", [], False)

checkProposalPps :: Maybe String -> (String, [PersistValue], Bool)
checkProposalPps Nothing = ("", [], True)
checkProposalPps (Just p)
  | "P" `isPrefixOf` p =
    (" and proposal=? ", [PersistText $ T.pack p], True)
  | otherwise = ("", [], False)

checkProposalAcc :: Maybe String -> (String, [PersistValue], Bool)
checkProposalAcc = checkBallotAcc

checkRevealAcc :: Maybe String -> (String, [PersistValue], Bool)
checkRevealAcc = checkProposalAcc

checkTxAcc :: Maybe String -> (String, [PersistValue], Bool)
checkTxAcc Nothing = ("", [], True)
checkTxAcc (Just a)
  | "tz" `isPrefixOf` a || "KT" `isPrefixOf` a =
    (" and (destination=? or source=?) ", replicate  2 $ PersistText $ T.pack a, True)
  | otherwise = ("", [], False)

checkDelegationAcc :: Maybe String -> (String, [PersistValue], Bool)
checkDelegationAcc Nothing = ("", [], True)
checkDelegationAcc (Just a)
  | "tz" `isPrefixOf` a || "KT" `isPrefixOf` a =
    (" and (delegate=? or source=?) ", replicate  2 $ PersistText $ T.pack a, True)
  | otherwise = ("", [], False)

checkOriginationAcc :: Maybe String -> (String, [PersistValue], Bool)
checkOriginationAcc Nothing = ("", [], True)
checkOriginationAcc (Just a)
  | "tz" `isPrefixOf` a || "KT" `isPrefixOf` a =
    (" and (delegate=? or source=? or manager_pubkey=? or ?=any(operation_result_originated_contracts)) "
    , replicate  4 $ PersistText $ T.pack a, True)
  | otherwise = ("", [], False)

checkEndorAcc :: Maybe String -> (String, [PersistValue], Bool)
checkEndorAcc Nothing = ("", [], True)
checkEndorAcc (Just a)
  | "tz" `isPrefixOf` a =
    (" and delegate =? ", [ PersistText $ T.pack a ], True)
  | otherwise = ("", [], False)

checkActivateAccountAcc :: Maybe String -> (String, [PersistValue], Bool)
checkActivateAccountAcc Nothing = ("", [], True)
checkActivateAccountAcc (Just a)
  | "tz" `isPrefixOf` a || "KT" `isPrefixOf` a=
    (" and pkh=? ", [ PersistText $ T.pack a ], True)
  | otherwise = ("", [], False)

checkOp' :: String -> ([PersistValue], Bool)
checkOp' o
   | "o" `isPrefixOf` o = ([PersistText $ T.pack o], True)
   | otherwise = ([], False)
