{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE RankNTypes                 #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE StandaloneDeriving         #-}
{-# LANGUAGE EmptyDataDecls             #-}
{-# LANGUAGE DeriveDataTypeable         #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE DuplicateRecordFields      #-}

module Mooncake.TezosId.V1.DBModels where

import Mooncake.Common.UtilityTH

import qualified Data.Text as T
import Data.Int

import Database.Groundhog.TH
import Database.Groundhog.Postgresql.Array as G

import GHC.Generics (Generic)
import qualified Data.Aeson as A

---
data BlockDetail = L {
      block        :: Blocks
    , blocksAlpha  :: BlockAlphas
    , blockStat    :: BlockStats
    }
    deriving (Show, Generic)

instance A.ToJSON BlockDetail

data OpTxsDetail = T {
    op                 :: Ops
  , tx                 :: OpTxs
  , internalTx         :: [ InternalOpTxs ]
  , internalDelegation :: [ InternalOpDelegations ]
} deriving (Show, Generic)

instance A.ToJSON OpTxsDetail

data OpEndorsDetail = E {
    op       :: Ops
  , endor    :: OpEndorments
} deriving (Show, Generic)

instance A.ToJSON OpEndorsDetail

data OpDelegationsDetail = D {
    op           :: Ops
  , delegation   :: OpDelegations
} deriving (Show, Generic)

instance A.ToJSON OpDelegationsDetail

data OpOriginationsDetail = O {
    op          :: Ops
  , origination :: OpOriginations
} deriving (Show, Generic)

instance A.ToJSON OpOriginationsDetail

data OpBallotsDetail = B {
    op          :: Ops
  , ballot      :: OpBallots
} deriving (Show, Generic)

instance A.ToJSON OpBallotsDetail

data OpProposalsDetail = P {
    op          :: Ops
  , proposal    :: OpProposals
} deriving (Show, Generic)

instance A.ToJSON OpProposalsDetail

data OpActivateAccountsDetail = A {
    op               :: Ops
  , activateAccount  :: OpActivateAccounts
} deriving (Show, Generic)

instance A.ToJSON OpActivateAccountsDetail

data OpRevealsDetail = R {
    op      :: Ops
  , reveal  :: OpReveals
} deriving (Show, Generic)

instance A.ToJSON OpRevealsDetail

data OpSeedNonceRevelationsDetail = S {
    op                  :: Ops
  , seedNonceEvelation  :: OpSeedNonceRevelations
} deriving (Show, Generic)

instance A.ToJSON OpSeedNonceRevelationsDetail

data OpDoubleBakingEvidencesDetail = DBE {
    op                    :: Ops
  , doubleBakingEvidences :: OpDoubleBakingEvidences
  , bh1s                  :: Bh1s
  , bh2s                  :: Bh2s
} deriving (Show, Generic)
instance A.ToJSON OpDoubleBakingEvidencesDetail

data OpDoubleEndorsementEvidencesDetail = DEE {
    op                         :: Ops
  , doubleEndorsementEvidences :: OpDoubleEndorsementEvidences
  , op1s                       :: Op1s
  , op2s                       :: Op2s
} deriving (Show, Generic)
instance A.ToJSON OpDoubleEndorsementEvidencesDetail

data Stat = Stat {
    blocks :: Count
  , txs    :: Count
} deriving (Show, Generic)
instance A.ToJSON Stat
---

data Count = C Int
    deriving (Show, Generic)

instance A.ToJSON Count

data Protocols = Protocols {
                protocol :: T.Text
              , proto    :: Double
              , from     :: Int
              , to       :: Int
              } deriving (Show, Generic)

instance A.ToJSON Protocols

data Proposers = Proposers {
      proposal            :: T.Text
    , count               :: Int
    , period              :: Maybe Double
    , source              :: Maybe T.Text
    , blockLevel          :: Maybe Double
    , blockHash           :: Maybe T.Text
    , blockTimestamp      :: Maybe T.Text
    } deriving (Show, Generic)

instance A.ToJSON Proposers

---

instance A.ToJSON Ops
instance A.ToJSON Blocks
instance A.ToJSON BlockAlphas
instance A.ToJSON OpTxs
instance A.ToJSON OpEndorments
instance A.ToJSON OpDelegations
instance A.ToJSON OpOriginations
instance A.ToJSON OpBallots
instance A.ToJSON OpProposals
instance A.ToJSON OpActivateAccounts
instance A.ToJSON OpReveals
instance A.ToJSON OpSeedNonceRevelations
instance A.ToJSON OpDoubleBakingEvidences
instance A.ToJSON OpDoubleEndorsementEvidences
instance A.ToJSON InternalOpTxs
instance A.ToJSON InternalOpDelegations
instance A.ToJSON Bh1s
instance A.ToJSON Bh2s
instance A.ToJSON Op1s
instance A.ToJSON Op2s
instance A.ToJSON BlockStats
instance A.ToJSON Accounts
instance A.ToJSON TulipToolsCirculating
instance A.ToJSON TulipToolsTotal

---

data TulipToolsCirculating = TulipToolsCirculating
    { circulating         :: Maybe Double
    , insertedTimestamp   :: T.Text
    } deriving (Show, Generic)

data TulipToolsTotal = TulipToolsTotal
    { insertedTimestamp   :: T.Text
    , total               :: Maybe Double
    } deriving (Show, Generic)

data Accounts = Accounts
    { account   :: T.Text
    } deriving (Show, Generic)

data BlockStats = BlockStats
    { opRevealsFee             :: Maybe Int64
    , opDelegationsFee         :: Maybe Int64
    , opTxsFee                 :: Maybe Int64
    , opOriginationsFee        :: Maybe Int64
    , opRevealsCount           :: Maybe Int64
    , opDelegationsCount       :: Maybe Int64
    , opTxsCount               :: Maybe Int64
    , opOriginationsCount      :: Maybe Int64
    , opsCount                 :: Maybe Int64
    , opTxsVolume              :: Maybe Int64
    , level                    :: Maybe Double
    } deriving (Show, Generic)

data Op1s = Op1s
    { branch                           :: Maybe T.Text
    , doubleEndorsementEvidencesUuid   :: Maybe T.Text
    , insertedTimestamp                :: Maybe T.Text
    , kind                             :: Maybe T.Text
    , level                            :: Maybe Double
    , signature                        :: Maybe T.Text
    , uuid                             :: T.Text
    } deriving (Show, Generic)

data Op2s = Op2s
    { branch                           :: Maybe T.Text
    , doubleEndorsementEvidencesUuid   :: Maybe T.Text
    , insertedTimestamp                :: Maybe T.Text
    , kind                             :: Maybe T.Text
    , level                            :: Maybe Double
    , signature                        :: Maybe T.Text
    , uuid                             :: T.Text
    } deriving (Show, Generic)

data OpDoubleEndorsementEvidences = OpDoubleEndorsementEvidences
    { insertedTimestamp   :: Maybe T.Text
    , kind                :: Maybe T.Text
    , metadataUuid        :: Maybe T.Text
    , opUuid              :: Maybe T.Text
    , uuid                :: T.Text
    } deriving (Show, Generic)

data OpActivateAccounts = OpActivateAccounts
    { insertedTimestamp   :: T.Text
    , kind                :: Maybe T.Text
    , metadataUuid        :: Maybe T.Text
    , opUuid              :: Maybe T.Text
    , pkh                 :: Maybe T.Text
    , secret              :: Maybe T.Text
    , uuid                :: T.Text
    , blockLevel          :: Maybe Double
    , blockHash           :: Maybe T.Text
    , blockTimestamp      :: Maybe T.Text
    } deriving (Show, Generic)

data Ops = Ops
    { blockUuid           :: Maybe T.Text
    , branch              :: Maybe T.Text
    , chainId             :: Maybe T.Text
    , insertedTimestamp   :: T.Text
    , opHash              :: Maybe T.Text
    , protocol            :: Maybe T.Text
    , signature           :: Maybe T.Text
    , uuid                :: T.Text
    , blockLevel          :: Maybe Double
    , blockHash           :: Maybe T.Text
    , blockTimestamp      :: Maybe T.Text
    } deriving (Show, Generic)

data OpProposals = OpProposals
    { insertedTimestamp   :: T.Text
    , kind                :: Maybe T.Text
    , metadataUuid        :: Maybe T.Text
    , opUuid              :: Maybe T.Text
    , period              :: Maybe Double
    , proposals           :: Array T.Text
    , source              :: Maybe T.Text
    , uuid                :: T.Text
    , blockLevel          :: Maybe Double
    , blockHash           :: Maybe T.Text
    , blockTimestamp      :: Maybe T.Text
    } deriving (Show, Generic)

data Blocks = Blocks
    { chainId             :: Maybe T.Text
    , context             :: Maybe T.Text
    , fitness             :: Array T.Text
    , hash                :: Maybe T.Text
    , insertedTimestamp   :: T.Text
    , level               :: Maybe Double
    , operationsHash      :: Maybe T.Text
    , predecessor         :: Maybe T.Text
    , priority            :: Maybe Double
    , proofOfWorkNonce    :: Maybe T.Text
    , proto               :: Maybe Double
    , protocol            :: Maybe T.Text
    , seedNonceHash       :: Maybe T.Text
    , signature           :: Maybe T.Text
    , timestamp           :: Maybe T.Text
    , uuid                :: T.Text
    , validationPass      :: Maybe Double
    } deriving (Show, Generic)

data OpReveals = OpReveals
    { counter                      :: Maybe T.Text
    , fee                          :: Maybe T.Text
    , gasLimit                     :: Maybe T.Text
    , insertedTimestamp            :: T.Text
    , kind                         :: Maybe T.Text
    , metadataUuid                 :: Maybe T.Text
    , opUuid                       :: Maybe T.Text
    , operationResultConsumedGas   :: Maybe T.Text
    , operationResultErrors        :: Maybe T.Text
    , operationResultStatus        :: Maybe T.Text
    , operationResultUuid          :: Maybe T.Text
    , publicKey                    :: Maybe T.Text
    , source                       :: Maybe T.Text
    , storageLimit                 :: Maybe T.Text
    , uuid                         :: T.Text
    , blockLevel                   :: Maybe Double
    , blockHash                    :: Maybe T.Text
    , blockTimestamp               :: Maybe T.Text
    } deriving (Show, Generic)

data Balances = Balances
    { category            :: Maybe T.Text
    , change              :: Maybe T.Text
    , contract            :: Maybe T.Text
    , cycle               :: Maybe Double
    , delegate            :: Maybe T.Text
    , insertedTimestamp   :: T.Text
    , kind                :: Maybe T.Text
    , parentUuid          :: Maybe T.Text
    , uuid                :: T.Text
    } deriving (Show, Read, Generic)

data Bh1s = Bh1s
    { context                     :: Maybe T.Text
    , doubleBakingEvidencesUuid   :: Maybe T.Text
    , fitness                     :: Array T.Text
    , insertedTimestamp           :: T.Text
    , level                       :: Maybe Double
    , operationsHash              :: Maybe T.Text
    , predecessor                 :: Maybe T.Text
    , priority                    :: Maybe Double
    , proofOfWorkNonce            :: Maybe T.Text
    , proto                       :: Maybe Double
    , signature                   :: Maybe T.Text
    , timestamp                   :: Maybe T.Text
    , uuid                        :: T.Text
    , validationPass              :: Maybe Double
    } deriving (Show, Generic)

data Bh2s = Bh2s
    { context                     :: Maybe T.Text
    , doubleBakingEvidencesUuid   :: Maybe T.Text
    , fitness                     :: Array T.Text
    , insertedTimestamp           :: T.Text
    , level                       :: Maybe Double
    , operationsHash              :: Maybe T.Text
    , predecessor                 :: Maybe T.Text
    , priority                    :: Maybe Double
    , proofOfWorkNonce            :: Maybe T.Text
    , proto                       :: Maybe Double
    , signature                   :: Maybe T.Text
    , timestamp                   :: Maybe T.Text
    , uuid                        :: T.Text
    , validationPass              :: Maybe Double
    } deriving (Show, Generic)

data BlockAlphas = BlockAlphas
    { baker                    :: Maybe T.Text
    , blockUuid                :: Maybe T.Text
    , consumedGas              :: Maybe T.Text
    , cycle                    :: Maybe Double
    , cyclePosition            :: Maybe Double
    , expectedCommitment       :: Maybe Bool
    , insertedTimestamp        :: T.Text
    , level                    :: Maybe Double
    , levelPosition            :: Maybe Double
    , maxBlockHeaderLength     :: Maybe Double
    , maxOperationDataLength   :: Maybe Double
    , maxOperationsTtl         :: Maybe Double
    , nextProtocol             :: Maybe T.Text
    , nonceHash                :: Maybe T.Text
    , protocol                 :: Maybe T.Text
    , testChainChainId         :: Maybe T.Text
    , testChainExpiration      :: Maybe T.Text
    , testChainGenesis         :: Maybe T.Text
    , testChainParentUuid      :: Maybe T.Text
    , testChainProtocol        :: Maybe T.Text
    , testChainStatus          :: Maybe T.Text
    , uuid                     :: T.Text
    , votingPeriod             :: Maybe Double
    , votingPeriodKind         :: Maybe T.Text
    , votingPeriodPosition     :: Maybe Double
    } deriving (Show, Generic)

data OpDelegations = OpDelegations
    { counter                      :: Maybe T.Text
    , delegate                     :: Maybe T.Text
    , fee                          :: Maybe T.Text
    , gasLimit                     :: Maybe T.Text
    , insertedTimestamp            :: T.Text
    , kind                         :: Maybe T.Text
    , metadataUuid                 :: Maybe T.Text
    , opUuid                       :: Maybe T.Text
    , operationResultConsumedGas   :: Maybe T.Text
    , operationResultErrors        :: Maybe T.Text
    , operationResultStatus        :: Maybe T.Text
    , operationResultUuid          :: Maybe T.Text
    , source                       :: Maybe T.Text
    , storageLimit                 :: Maybe T.Text
    , uuid                         :: T.Text
    } deriving (Show, Generic)

data InternalOpDelegations = InternalOpDelegations
    { delegate            :: Maybe T.Text
    , insertedTimestamp   :: T.Text
    , kind                :: Maybe T.Text
    , nonce               :: Maybe Double
    , opMetadataUuid      :: Maybe T.Text
    , resultConsumedGas   :: Maybe T.Text
    , resultErrors        :: Maybe T.Text
    , resultStatus        :: Maybe T.Text
    , resultUuid          :: Maybe T.Text
    , source              :: Maybe T.Text
    , uuid                :: T.Text
    } deriving (Show, Generic)

data InternalOpTxs = InternalOpTxs
    { amount                      :: Maybe T.Text
    , destination                 :: Maybe T.Text
    , insertedTimestamp           :: T.Text
    , kind                        :: Maybe T.Text
    , nonce                       :: Maybe Double
    , opMetadataUuid              :: Maybe T.Text
    , parameters                  :: Maybe T.Text
    , resultBigMapDiff            :: Maybe T.Text
    , resultConsumedGas           :: Maybe T.Text
    , resultErrors                :: Maybe T.Text
    , resultPaidStorageSizeDiff   :: Maybe T.Text
    , resultStatus                :: Maybe T.Text
    , resultStorage               :: Maybe T.Text
    , resultStorageSize           :: Maybe T.Text
    , resultUuid                  :: Maybe T.Text
    , source                      :: Maybe T.Text
    , uuid                        :: T.Text
    } deriving (Show, Generic)

data OpTxs = OpTxs
    { amount                                        :: Maybe T.Text
    , counter                                       :: Maybe T.Text
    , destination                                   :: Maybe T.Text
    , fee                                           :: Maybe T.Text
    , gasLimit                                      :: Maybe T.Text
    , insertedTimestamp                             :: T.Text
    , kind                                          :: Maybe T.Text
    , metadataUuid                                  :: Maybe T.Text
    , opUuid                                        :: Maybe T.Text
    , operationResultAllocatedDestinationContract   :: Maybe Bool
    , operationResultBigMapDiff                     :: Maybe T.Text
    , operationResultConsumedGas                    :: Maybe T.Text
    , operationResultErrors                         :: Maybe T.Text
    , operationResultPaidStorageSizeDiff            :: Maybe T.Text
    , operationResultStatus                         :: Maybe T.Text
    , operationResultStorage                        :: Maybe T.Text
    , operationResultStorageSize                    :: Maybe T.Text
    , operationResultUuid                           :: Maybe T.Text
    , parameters                                    :: Maybe T.Text
    , source                                        :: Maybe T.Text
    , storageLimit                                  :: Maybe T.Text
    , uuid                                          :: T.Text
    , blockLevel                                    :: Maybe Double
    , blockHash                                     :: Maybe T.Text
    , blockTimestamp                                :: Maybe T.Text
    } deriving (Show, Generic)

data OpOriginations = OpOriginations
    { balance                              :: Maybe T.Text
    , counter                              :: Maybe T.Text
    , delegatable                          :: Maybe Bool
    , delegate                             :: Maybe T.Text
    , fee                                  :: Maybe T.Text
    , gasLimit                             :: Maybe T.Text
    , insertedTimestamp                    :: T.Text
    , kind                                 :: Maybe T.Text
    , managerPubkey                        :: Maybe T.Text
    , metadataUuid                         :: Maybe T.Text
    , opUuid                               :: Maybe T.Text
    , operationResultConsumedGas           :: Maybe T.Text
    , operationResultErrors                :: Maybe T.Text
    , operationResultOriginatedContracts   :: Array T.Text
    , operationResultPaidStorageSizeDiff   :: Maybe T.Text
    , operationResultStatus                :: Maybe T.Text
    , operationResultStorageSize           :: Maybe T.Text
    , operationResultUuid                  :: Maybe T.Text
    , script                               :: Maybe T.Text
    , source                               :: Maybe T.Text
    , spendable                            :: Maybe Bool
    , storageLimit                         :: Maybe T.Text
    , uuid                                 :: T.Text
    } deriving (Show, Generic)

data OpDoubleBakingEvidences = OpDoubleBakingEvidences
    { insertedTimestamp   :: T.Text
    , kind                :: Maybe T.Text
    , metadataUuid        :: Maybe T.Text
    , opUuid              :: Maybe T.Text
    , uuid                :: T.Text
    , blockLevel          :: Maybe Double
    , blockHash           :: Maybe T.Text
    , blockTimestamp      :: Maybe T.Text
    } deriving (Show, Generic)

data OpSeedNonceRevelations = OpSeedNonceRevelations
    { insertedTimestamp   :: T.Text
    , kind                :: Maybe T.Text
    , level               :: Maybe Double
    , metadataUuid        :: Maybe T.Text
    , nonce               :: Maybe T.Text
    , opUuid              :: Maybe T.Text
    , uuid                :: T.Text
    , blockLevel          :: Maybe Double
    , blockHash           :: Maybe T.Text
    , blockTimestamp      :: Maybe T.Text
    } deriving (Show, Generic)

data OpEndorments = OpEndorments
    { delegate            :: Maybe T.Text
    , insertedTimestamp   :: T.Text
    , kind                :: Maybe T.Text
    , level               :: Maybe Double
    , metadataUuid        :: Maybe T.Text
    , opUuid              :: Maybe T.Text
    , slots               :: Array Double
    , uuid                :: T.Text
    , blockLevel          :: Maybe Double
    , blockHash           :: Maybe T.Text
    , blockTimestamp      :: Maybe T.Text
    } deriving (Show, Generic)

data OpBallots = OpBallots
    { ballot              :: Maybe T.Text
    , insertedTimestamp   :: T.Text
    , kind                :: Maybe T.Text
    , metadataUuid        :: Maybe T.Text
    , opUuid              :: Maybe T.Text
    , period              :: Maybe Double
    , proposal            :: Maybe T.Text
    , source              :: Maybe T.Text
    , uuid                :: T.Text
    , blockLevel          :: Maybe Double
    , blockHash           :: Maybe T.Text
    , blockTimestamp      :: Maybe T.Text
    } deriving (Show, Generic)

mkPersist codegenConfig [groundhog|
- entity: Balances
  dbName: balances
  constructors:
    - name: Balances
      fields:
        - name: insertedTimestamp
          dbName: _inserted_timestamp
          type: "timestamp without time zone"

- entity: TulipToolsTotal
  dbName: tulip_tools_total
  constructors:
    - name: TulipToolsTotal
      fields:
        - name: insertedTimestamp
          dbName: _inserted_timestamp
          type: "timestamp without time zone"

- entity: TulipToolsCirculating
  dbName: tulip_tools_circulating
  constructors:
    - name: TulipToolsCirculating
      fields:
        - name: insertedTimestamp
          dbName: _inserted_timestamp
          type: "timestamp without time zone"

- entity: Bh1s
  dbName: bh1s
  constructors:
    - name: Bh1s
      fields:
        - name: insertedTimestamp
          dbName: _inserted_timestamp
          type: "timestamp without time zone"

- entity: Bh2s
  dbName: bh2s
  constructors:
    - name: Bh2s
      fields:
        - name: insertedTimestamp
          dbName: _inserted_timestamp
          type: "timestamp without time zone"

- entity: BlockAlphas
  dbName: block_alphas
  constructors:
    - name: BlockAlphas
      fields:
        - name: insertedTimestamp
          dbName: _inserted_timestamp
          type: "timestamp without time zone"

- entity: Blocks
  dbName: blocks
  constructors:
    - name: Blocks
      fields:
        - name: insertedTimestamp
          dbName: _inserted_timestamp
          type: "timestamp without time zone"

- entity: InternalOpDelegations
  dbName: internal_op_delegations
  constructors:
    - name: InternalOpDelegations
      fields:
        - name: insertedTimestamp
          dbName: _inserted_timestamp
          type: "timestamp without time zone"

- entity: InternalOpTxs
  dbName: internal_op_txs
  constructors:
    - name: InternalOpTxs
      fields:
        - name: insertedTimestamp
          dbName: _inserted_timestamp
          type: "timestamp without time zone"

- entity: OpActivateAccounts
  dbName: op_activate_accounts
  constructors:
    - name: OpActivateAccounts
      fields:
        - name: insertedTimestamp
          dbName: _inserted_timestamp
          type: "timestamp without time zone"

- entity: OpBallots
  dbName: op_ballots
  constructors:
    - name: OpBallots
      fields:
        - name: insertedTimestamp
          dbName: _inserted_timestamp
          type: "timestamp without time zone"

- entity: OpDelegations
  dbName: op_delegations
  constructors:
    - name: OpDelegations
      fields:
        - name: insertedTimestamp
          dbName: _inserted_timestamp
          type: "timestamp without time zone"

- entity: OpDoubleEndorsementEvidences
  dbName: op_double_endorsement_evidences
  constructors:
    - name: OpDoubleEndorsementEvidences
      fields:
        - name: insertedTimestamp
          dbName: _inserted_timestamp
          type: "timestamp without time zone"

- entity: Op2s
  dbName: op2s
  constructors:
    - name: Op2s
      fields:
        - name: insertedTimestamp
          dbName: _inserted_timestamp
          type: "timestamp without time zone"

- entity: Op1s
  dbName: op1s
  constructors:
    - name: Op1s
      fields:
        - name: insertedTimestamp
          dbName: _inserted_timestamp
          type: "timestamp without time zone"

- entity: OpDoubleBakingEvidences
  dbName: op_double_baking_evidences
  constructors:
    - name: OpDoubleBakingEvidences
      fields:
        - name: insertedTimestamp
          dbName: _inserted_timestamp
          type: "timestamp without time zone"

- entity: OpEndorments
  dbName: op_endorments
  constructors:
    - name: OpEndorments
      fields:
        - name: insertedTimestamp
          dbName: _inserted_timestamp
          type: "timestamp without time zone"

- entity: OpOriginations
  dbName: op_originations
  constructors:
    - name: OpOriginations
      fields:
        - name: insertedTimestamp
          dbName: _inserted_timestamp
          type: "timestamp without time zone"

- entity: OpProposals
  dbName: op_proposals
  constructors:
    - name: OpProposals
      fields:
        - name: insertedTimestamp
          dbName: _inserted_timestamp
          type: "timestamp without time zone"

- entity: OpReveals
  dbName: op_reveals
  constructors:
    - name: OpReveals
      fields:
        - name: insertedTimestamp
          dbName: _inserted_timestamp
          type: "timestamp without time zone"

- entity: OpSeedNonceRevelations
  dbName: op_seed_nonce_revelations
  constructors:
    - name: OpSeedNonceRevelations
      fields:
        - name: insertedTimestamp
          dbName: _inserted_timestamp
          type: "timestamp without time zone"

- entity: OpTxs
  dbName: op_txs
  constructors:
    - name: OpTxs
      fields:
        - name: insertedTimestamp
          dbName: _inserted_timestamp
          type: "timestamp without time zone"

- entity: Ops
  dbName: ops
  constructors:
    - name: Ops
      fields:
        - name: insertedTimestamp
          dbName: _inserted_timestamp
          type: "timestamp without time zone"

- entity: BlockStats
  dbName: block_stats
  constructors:
    - name: BlockStats

- entity: Accounts
  dbName: accounts
  constructors:
    - name: Accounts

- entity: Count
- entity: Proposers
- entity: Protocols
- entity: Stat
|]
