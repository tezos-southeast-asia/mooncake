{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE TypeOperators              #-}

module Mooncake.TezosId.V0.Server
  ( APIV0
  , serverV0
  ) where

import Mooncake.TezosId.V0.App as TV0
import Mooncake.TezosId.V0.Models as TV0
import Mooncake.Common.Config

import Data.Maybe

import Database.Persist
import Database.Persist.MySQL

import Control.Monad.IO.Class  (liftIO)

import Servant

type APIV0 =
  "v0" :> "operations" :> QueryParam "type" String
                       :> QueryParam "p" Int
                       :> QueryParam "n" Int
                       :> Get '[JSON] [ Entity Operation ]
  :<|> "v0" :> "blocks" :> QueryParam "p" Int
                        :> QueryParam "n" Int
                        :> Get '[JSON] [ BlockInfo ]

  :<|> "v0" :> "block_operation" :> Capture "hash" String
                                 :> QueryParam "p" Int
                                 :> QueryParam "n" Int
                                 :> Get '[JSON] [ Entity Operation ]
  :<|> "v0" :> "operation" :> Capture "hash" String
                           :> QueryParam "p" Int
                           :> QueryParam "n" Int
                           :> Get '[JSON] [ Entity Operation ]
  :<|> "v0" :> "block" :> Capture "hash" String
                       :> Get '[JSON] (Maybe BlockInfo)
  :<|> "v0" :> "account" :> Capture "hash" String
                         :> QueryParam "p" Int
                         :> QueryParam "n" Int
                         :> Get '[JSON] [ Entity Operation ]
  :<|> "v0" :> "peers" :> QueryParam "p" Int
                        :> QueryParam "n" Int
                        :> Get '[JSON] [ Entity Peerid ]
  :<|> "v0" :> "protocols" :> QueryParam "proto" Int
                        :> Get '[JSON] [ ProtoPeriod ]
  :<|> "v0" :> "account_transactions" :> Capture "hash" String
                                      :> QueryParam "p" Int
                                      :> QueryParam "n" Int
                                      :> Get '[JSON] [ AccTxn ]
  :<|> "v0" :> "account_transactions_num" :> Capture "hash" String
                                          :> Get '[JSON] Int
  :<|> "v0" :> "account_delegations" :> Capture "hash" String
                                     :> QueryParam "p" Int
                                     :> QueryParam "n" Int
                                     :> Get '[JSON] [ AccDel ]
  :<|> "v0" :> "account_delegations_num" :> Capture "hash" String
                                         :> Get '[JSON] Int
  :<|> "v0" :> "account_originations" :> Capture "hash" String
                                      :> QueryParam "p" Int
                                      :> QueryParam "n" Int
                                      :> Get '[JSON] [ AccOri ]
  :<|> "v0" :> "account_originations_num" :> Capture "hash" String
                                          :> Get '[JSON] Int
  :<|> "v0" :> "account_endorsements" :> Capture "hash" String
                                      :> QueryParam "p" Int
                                      :> QueryParam "n" Int
                                      :> Get '[JSON] [ AccEnd ]
  :<|> "v0" :> "account_endorsements_num" :> Capture "hash" String
                                          :> Get '[JSON] Int
  :<|> "v0" :> "hashType" :> Capture "hash" String
                          :> Get '[JSON] (Maybe HashType)

serverV0 :: ConnectionPool -> Server APIV0
serverV0 mp =   operationsV0 mp
             :<|> blocksV0 mp
             :<|> blockOperationV0 mp
             :<|> operationV0 mp
             :<|> blockV0 mp
             :<|> accountV0 mp
             :<|> peersV0 mp
             :<|> protocolsV0 mp
             :<|> accountTransactionV0 mp
             :<|> accountTransactionNumV0 mp
             :<|> accountDelegationV0 mp
             :<|> accountDelegationNumV0 mp
             :<|> accountOriginationV0 mp
             :<|> accountOriginationNumV0 mp
             :<|> accountEndorsementV0 mp
             :<|> accountEndorsementNumV0 mp
             :<|> hashType

operationsV0 :: ConnectionPool
             -> Maybe String  -- ^ type
             -> Maybe Int     -- ^ page
             -> Maybe Int     -- ^ number
             -> Handler [Entity Operation]
operationsV0 p mty mpg mnu = liftIO $ lastesOperations p mty pg nu
    where pg = fromMaybe defaultPage mpg
          nu = fromMaybe defaultNumberPerPage mnu

blockOperationV0 :: ConnectionPool
             -> String        -- ^ hash
             -> Maybe Int     -- ^ page
             -> Maybe Int     -- ^ number
             -> Handler [Entity Operation]
blockOperationV0 p h mpg mnu = liftIO $ blockOperation p h pg nu
    where pg = fromMaybe defaultPage mpg
          nu = fromMaybe defaultNumberPerPage mnu

blocksV0 :: ConnectionPool
             -> Maybe Int     -- ^ page
             -> Maybe Int     -- ^ number
             -> Handler [ BlockInfo ]
blocksV0 p mpg mnu = liftIO $ latestBlocks p pg nu
    where pg = fromMaybe defaultPage mpg
          nu = fromMaybe defaultNumberPerPage mnu

operationV0 :: ConnectionPool
            -> String
            -> Maybe Int     -- ^ page
            -> Maybe Int     -- ^ number
            -> Handler [ Entity Operation ]
operationV0 p h mpg mnu = liftIO $ operationInfo p h pg nu
    where pg = fromMaybe defaultPage mpg
          nu = fromMaybe defaultNumberPerPage mnu

blockV0 :: ConnectionPool
        -> String
        -> Handler (Maybe BlockInfo)
blockV0 p h = liftIO $ blockInfo p h

accountV0 :: ConnectionPool
            -> String
            -> Maybe Int     -- ^ page
            -> Maybe Int     -- ^ number
            -> Handler [ Entity Operation ]
accountV0 p h mpg mnu = liftIO $ TV0.account p h pg nu
    where pg = fromMaybe defaultPage mpg
          nu = fromMaybe defaultNumberPerPage mnu

peersV0 :: ConnectionPool
        -> Maybe Int     -- ^ page
        -> Maybe Int     -- ^ number
        -> Handler [ Entity Peerid ]
peersV0 p mpg mnu = liftIO $ peers p pg nu
    where pg = fromMaybe defaultPage mpg
          nu = fromMaybe defaultNumberPerPage mnu

protocolsV0 :: ConnectionPool
        -> Maybe Int     -- ^ protocols
        -> Handler [ProtoPeriod]
protocolsV0 p mr = liftIO $ protocols p r
    where r = fromMaybe 3 mr

accountTransactionV0 :: ConnectionPool
            -> String
            -> Maybe Int     -- ^ page
            -> Maybe Int     -- ^ number
            -> Handler [ AccTxn ]
accountTransactionV0 p h mpg mnu = liftIO $ account_transaction p h pg nu
    where pg = fromMaybe defaultPage mpg
          nu = fromMaybe defaultNumberPerPage mnu

accountTransactionNumV0 :: ConnectionPool
                        -> String
                        -> Handler Int
accountTransactionNumV0 p s = liftIO $ account_transaction_num p s

accountDelegationV0 :: ConnectionPool
            -> String
            -> Maybe Int     -- ^ page
            -> Maybe Int     -- ^ number
            -> Handler [ AccDel ]
accountDelegationV0 p h mpg mnu = liftIO $ account_delegation p h pg nu
    where pg = fromMaybe defaultPage mpg
          nu = fromMaybe defaultNumberPerPage mnu

accountDelegationNumV0 :: ConnectionPool
                        -> String
                        -> Handler Int
accountDelegationNumV0 p s = liftIO $ account_delegation_num p s

accountOriginationV0 :: ConnectionPool
            -> String
            -> Maybe Int     -- ^ page
            -> Maybe Int     -- ^ number
            -> Handler [ AccOri ]
accountOriginationV0 p h mpg mnu = liftIO $ account_origination p h pg nu
    where pg = fromMaybe defaultPage mpg
          nu = fromMaybe defaultNumberPerPage mnu

accountOriginationNumV0 :: ConnectionPool
                        -> String
                        -> Handler Int
accountOriginationNumV0 p s = liftIO $ account_origination_num p s

accountEndorsementV0 :: ConnectionPool
            -> String
            -> Maybe Int     -- ^ page
            -> Maybe Int     -- ^ number
            -> Handler [ AccEnd ]
accountEndorsementV0 p h mpg mnu = liftIO $ account_endorsement p h pg nu
    where pg = fromMaybe defaultPage mpg
          nu = fromMaybe defaultNumberPerPage mnu

accountEndorsementNumV0 :: ConnectionPool
                        -> String
                        -> Handler Int
accountEndorsementNumV0 p s = liftIO $ account_endorsement_num p s

hashType :: String
         -> Handler (Maybe HashType)
hashType h = return $ lookupHashType h
