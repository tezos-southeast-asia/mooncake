{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE TypeOperators              #-}

module Mooncake.TezosId.V0.App
  ( lastesOperations
  , latestBlocks
  , lookupHashType
  , blockInfo
  , account
  , operationInfo
  , blockOperation
  , peers
  , protocols
  , account_transaction_type
  , account_origination_type
  , account_delegation_type
  , account_endorsement_type
  , account_transaction
  , account_transaction_num
  , account_delegation
  , account_delegation_num
  , account_origination
  , account_origination_num
  , account_endorsement
  , account_endorsement_num
  , BlockInfo ()
  , ProtoPeriod ()
  , AccTxn ()
  , AccDel ()
  , AccOri ()
  , AccEnd ()
  , HashType
  ) where

import qualified Data.Aeson as A
import Data.Maybe
import GHC.Generics

import Database.Persist
import Database.Persist.MySQL
import qualified Database.Esqueleto as E
import qualified Data.Text as T

import Mooncake.Common.Config
import Mooncake.Common.UtilityTH
import Mooncake.TezosId.V0.Models

lastesOperations :: ConnectionPool
                  -> Maybe String   -- ^ Type
                  -> Int      -- ^ Page
                  -> Int      -- ^ Number
                  -> IO [Entity Operation]
lastesOperations pool mtp pg nu = flip runSqlPersistMPool pool $ do
  let filter' = maybe [] (\x -> [OperationKind ==. x]) mtp
  selectList filter'
             [ Desc OperationUnix
             , OffsetBy $ offset pg nu
             , LimitTo $ safeNumberPerPage nu
             ]

data BlockInfo = B
           { block :: Entity Block
           , baker :: Maybe (Entity Baker)
           } deriving (Show, Generic)

instance A.ToJSON BlockInfo
instance A.FromJSON BlockInfo

latestBlocks :: ConnectionPool
             -> Int      -- ^ Page
             -> Int      -- ^ Number
             -> IO [ BlockInfo ]
latestBlocks pool pg nu = flip runSqlPersistMPool pool $ do
    fmap (map (\(x,y) -> B x y)) $ E.select $
      E.from $ \(bo `E.LeftOuterJoin` mbk) -> do
        E.on (E.just (bo E.^. BlockId) E.==. mbk E.?. BakerBlockId)
        E.orderBy [ E.desc (bo E.^. BlockUnix) ]
        E.limit $ toInt $ safeNumberPerPage nu
        E.offset $ toInt $ offset pg nu
        return (bo, mbk)

data HashType = Block_
              | Account_
              | Operation_
              deriving (Generic, Show)

instance A.ToJSON HashType where
    toJSON ht = A.toJSON $ init $ show ht

lookupHashType :: String   -- ^ could be block, account(KT1/tz1), op
               -> Maybe HashType
lookupHashType hs
  | take 1 hs == "B" && length hs == 51 = Just Block_
  | take 3 hs == "KT1" && length hs == 36 = Just Account_
  | take 3 hs == "tz1" && length hs == 36 = Just Account_
  | take 1 hs == "o" && length hs == 51 = Just Operation_
  | otherwise = Nothing

blockInfo :: ConnectionPool
      -> String
      -> IO (Maybe BlockInfo)
blockInfo pool hs = flip runSqlPersistMPool pool $ do
    fmap (listToMaybe . map (\(x,y) -> B x y)) $ E.select $
      E.from $ \(bo `E.LeftOuterJoin` mbk) -> do
        E.where_ (bo E.^. BlockHash E.==. E.val hs)
        E.on (E.just (bo E.^. BlockId) E.==. mbk E.?. BakerBlockId)
        return (bo, mbk)

account :: ConnectionPool
        -> String   -- ^ Hash
        -> Int      -- ^ Page
        -> Int      -- ^ Number
        -> IO [Entity Operation]
account pool hs pg nu = flip runSqlPersistMPool pool $ do
  selectList (   [OperationSource ==. hs]
             ||. [OperationDestination ==. hs] )
             [ OffsetBy $ offset pg nu
             , LimitTo $ safeNumberPerPage nu
             ]

operationInfo :: ConnectionPool
          -> String
          -> Int      -- ^ Page
          -> Int      -- ^ Number
          -> IO [ Entity Operation ]
operationInfo pool hs pg nu = flip runSqlPersistMPool pool $ do
  selectList [OperationHash ==. hs]
             [ OffsetBy $ offset pg nu
             , LimitTo $ safeNumberPerPage nu
             ]

blockOperation :: ConnectionPool
          -> String
          -> Int      -- ^ Page
          -> Int      -- ^ Number
          -> IO [ Entity Operation ]
blockOperation pool hs pg nu = flip runSqlPersistMPool pool $ do
  selectList [OperationInblock ==. hs]
             [ OffsetBy $ offset pg nu
             , LimitTo $ safeNumberPerPage nu
             ]

peers :: ConnectionPool
      -> Int      -- ^ Page
      -> Int      -- ^ Number
      -> IO [ Entity Peerid ]
peers pool pg nu = flip runSqlPersistMPool pool $ do
  E.select $ E.from $ \p1 -> do
    E.where_ (E.just (p1 E.^. PeeridTimestamp)  E.==.
      ( E.sub_select $ E.from $ \p2 -> do
        return (E.max_ (p2 E.^. PeeridTimestamp))))
    E.limit $ toInt $ safeNumberPerPage nu
    E.offset $ toInt $ offset pg nu
    return p1

data ProtoPeriod = P
                 { min :: Int
                 , max :: Int
                 } deriving (Show, Generic)

instance A.ToJSON ProtoPeriod
instance A.FromJSON ProtoPeriod

protocols :: ConnectionPool
          -> Int  -- ^ proto
          -> IO [ProtoPeriod]
protocols pool r = flip runSqlPersistMPool pool $ do
  fmap (map (\(x,y) -> P (persistValeu2Int x) (persistValeu2Int y))) $
    E.rawSql "select min(level),max(level) from block where proto=?" [PersistInt64 $ toInt r]

data AccDel = AD { adOpHash    :: String
                 , adTimeStamp :: String
                 , adBlock     :: String
                 , adDelegatedAccount     :: String
                 , adFee     :: String
                 } deriving (Show, Generic)

data AccTxn = AT { atOpHash    :: String
                 , atTimeStamp :: String
                 , atFrom      :: String
                 , atTo        :: String
                 , atFee       :: String
                 , atAmount    :: String
                 } deriving (Show, Generic)

data AccOri = AO { aoOpHash    :: String
                 , aoTimeStamp :: String
                 , aoBlock     :: String
                 , aoOri     :: String
                 , aoFee     :: String
                 } deriving (Show, Generic)

data AccEnd = AE { aeOpHash    :: String
                 , aeTimeStamp :: String
                 , aeBlock     :: String
                 , aeSlots     :: String
                 } deriving (Show, Generic)

instance A.ToJSON AccTxn
instance A.ToJSON AccDel
instance A.ToJSON AccOri
instance A.ToJSON AccEnd

instance A.FromJSON AccTxn
instance A.FromJSON AccDel
instance A.FromJSON AccOri
instance A.FromJSON AccEnd

account_transaction_type :: ( Single PersistValue -> String
                            , Single PersistValue -> String
                            , Single PersistValue -> String
                            , Single PersistValue -> String
                            , Single PersistValue -> String
                            , Single PersistValue -> String)
account_transaction_type = ( persistValeu2String, persistValeu2String
                           , persistValeu2String, persistValeu2String
                           , persistValeu2String, persistValeu2String)

account_origination_type :: ( Single PersistValue -> String
                            , Single PersistValue -> String
                            , Single PersistValue -> String
                            , Single PersistValue -> String
                            , Single PersistValue -> String)
account_origination_type = account_delegation_type

account_delegation_type :: ( Single PersistValue -> String
                           , Single PersistValue -> String
                           , Single PersistValue -> String
                           , Single PersistValue -> String
                           , Single PersistValue -> String)
account_delegation_type = ( persistValeu2String, persistValeu2String
                           , persistValeu2String, persistValeu2String
                           , persistValeu2String)
account_endorsement_type :: ( Single PersistValue -> String
                            , Single PersistValue -> String
                            , Single PersistValue -> String
                            , Single PersistValue -> String)
account_endorsement_type = ( persistValeu2String, persistValeu2String
                           , persistValeu2String, persistValeu2String)

account_transaction :: ConnectionPool
                    -> String
                    -> Int      -- ^ Page
                    -> Int      -- ^ Number
                    -> IO [ AccTxn ]
account_transaction pool s pg nu = flip runSqlPersistMPool pool $ do
  fmap (map (\x -> $(uncurryN 6) AT $ f6 account_transaction_type x)) $
    E.rawSql ( T.unlines
                [ "SELECT hash, timestamp, source, destination, fee, amount"
                , "FROM operation WHERE (source = ? OR destination = ?) and kind = ? "
                , "ORDER BY timestamp DESC LIMIT ? OFFSET ?"
                ] ) [ PersistText $ T.pack s
                    , PersistText $ T.pack s
                    , PersistText $ "transaction"
                    , PersistInt64 $ toInt $ safeNumberPerPage nu
                    , PersistInt64 $ toInt $ offset pg nu
                    ]

account_transaction_num :: ConnectionPool
                        -> String
                        -> IO Int
account_transaction_num pool s = flip runSqlPersistMPool pool $ do
  fmap (head . map persistValeu2Int) $
    E.rawSql ( T.unlines
                [ "SELECT count(*) "
                , "FROM operation "
                , "WHERE (source = ? OR destination = ?) and kind = ? "
                ] ) [ PersistText $ T.pack s
                    , PersistText $ T.pack s
                    , PersistText $ "transaction"
                    ]

account_delegation :: ConnectionPool
                    -> String
                    -> Int      -- ^ Page
                    -> Int      -- ^ Number
                    -> IO [ AccDel ]
account_delegation pool s pg nu = flip runSqlPersistMPool pool $ do
  fmap (map (\x -> $(uncurryN 5) AD $ f5 account_delegation_type x)) $
    E.rawSql ( T.unlines
                [ "SELECT hash, timestamp, inblock, delegate, fee "
                , "FROM operation WHERE (source = ? OR delegate = ?) and kind = ?"
                , "ORDER BY timestamp DESC LIMIT ? OFFSET ?"
                ] ) [ PersistText $ T.pack s
                    , PersistText $ T.pack s
                    , PersistText $ "delegation"
                    , PersistInt64 $ toInt $ safeNumberPerPage nu
                    , PersistInt64 $ toInt $ offset pg nu
                    ]

account_delegation_num :: ConnectionPool
                        -> String
                        -> IO Int
account_delegation_num pool s = flip runSqlPersistMPool pool $ do
  fmap (head . map persistValeu2Int) $
    E.rawSql ( T.unlines
                [ "SELECT count(*) "
                , "FROM operation "
                , "WHERE  (source = ? OR delegate = ?) and kind = ?"
                ] ) [ PersistText $ T.pack s
                    , PersistText $ T.pack s
                    , PersistText $ "delegation"
                    ]

account_origination :: ConnectionPool
                    -> String
                    -> Int      -- ^ Page
                    -> Int      -- ^ Number
                    -> IO [ AccOri ]
account_origination pool s pg nu = flip runSqlPersistMPool pool $ do
  fmap (map (\x -> $(uncurryN 5) AO $ f5 account_origination_type x)) $
    E.rawSql ( T.unlines
                [ "SELECT hash, timestamp, inblock, source, fee "
                , "FROM operation WHERE (source = ? OR public_key = ? OR managerPubkey = ?) and kind = ?"
                , "ORDER BY timestamp DESC LIMIT ? OFFSET ?"
                ] ) [ PersistText $ T.pack s
                    , PersistText $ T.pack s
                    , PersistText $ T.pack s
                    , PersistText $ "origination"
                    , PersistInt64 $ toInt $ safeNumberPerPage nu
                    , PersistInt64 $ toInt $ offset pg nu
                    ]

account_origination_num :: ConnectionPool
                        -> String
                        -> IO Int
account_origination_num pool s = flip runSqlPersistMPool pool $ do
  fmap (head . map persistValeu2Int) $
    E.rawSql ( T.unlines
             [ "SELECT count(*) "
             , "FROM operation"
                , "WHERE  (source = ? OR public_key = ? OR managerPubkey = ?) and kind = ?"
                ] ) [ PersistText $ T.pack s
                    , PersistText $ T.pack s
                    , PersistText $ T.pack s
                    , PersistText $ "origination"
                    ]

account_endorsement :: ConnectionPool
                    -> String
                    -> Int      -- ^ Page
                    -> Int      -- ^ Number
                    -> IO [ AccEnd ]
account_endorsement pool s pg nu = flip runSqlPersistMPool pool $ do
  fmap (map (\x -> $(uncurryN 4) AE $ f4 account_endorsement_type x)) $
    E.rawSql ( T.unlines
                [ "SELECT hash, timestamp, inblock, slot "
                , "FROM operation WHERE delegate = ? and kind = ?"
                , "ORDER BY level DESC LIMIT ? OFFSET ?"
                ] ) [ PersistText $ T.pack s
                    , PersistText $ "endorsement"
                    , PersistInt64 $ toInt $ safeNumberPerPage nu
                    , PersistInt64 $ toInt $ offset pg nu
                    ]

account_endorsement_num :: ConnectionPool
                        -> String
                        -> IO Int
account_endorsement_num pool s = flip runSqlPersistMPool pool $ do
  fmap (head . map persistValeu2Int) $
    E.rawSql ( T.unlines
                [ "SELECT count(*) "
                , "FROM operation "
                , "WHERE delegate = ? and kind = ?"
                ] ) [ PersistText $ T.pack s
                    , PersistText $ "endorsement"
                    ]

toInt :: (Integral a, Integral b) => a -> b
toInt = fromInteger . toInteger

persistValeu2Int :: Single PersistValue -> Int
persistValeu2Int (Single PersistNull) = (-1)
persistValeu2Int (Single i) = either (const (-1)) id $ fromPersistValue i

persistValeu2String :: Single PersistValue -> String
persistValeu2String (Single PersistNull) = ""
persistValeu2String (Single i) = either (const "") id $ fromPersistValue i

applyT :: (a -> b, a) -> b
applyT (f, x) = f x

f4 :: ( b -> d, b -> d, b -> d, b -> d)
      -> (b, b, b, b) -> (d, d, d, d)
f4 p1 p2 = $(loopTN 4) applyT $ zipT4 p1 p2

zipT4 :: (a1, a2, a3, a4)
         -> (b1, b2, b3, b4) -> ((a1, b1), (a2, b2), (a3, b3), (a4, b4))
zipT4 (x1, x2, x3, x4) (y1, y2, y3, y4) = ((x1, y1), (x2, y2), (x3, y3), (x4, y4))

f5 :: (b -> e, b -> e, b -> e, b -> e, b -> e)
      -> (b, b, b, b, b) -> (e, e, e, e, e)
f5 p1 p2 = $(loopTN 5) applyT $ zipT5 p1 p2

zipT5 :: (a1, a2, a3, a4, a5)
      -> (b1, b2, b3, b4, b5)
      -> ((a1, b1), (a2, b2), (a3, b3), (a4, b4), (a5, b5))
zipT5 (x1, x2, x3, x4, x5) (y1, y2, y3, y4, y5) = ((x1, y1), (x2, y2), (x3, y3), (x4, y4), (x5, y5))

f6 :: (b -> f, b -> f, b -> f, b -> f, b -> f, b -> f)
   -> (b, b, b, b, b, b) -> (f, f, f, f, f, f)
f6 p1 p2 = $(loopTN 6) applyT $ zipT6 p1 p2

zipT6 :: (a1, a2, a3, a4, a5, a6)
      -> (b1, b2, b3, b4, b5, b6)
      -> ((a1, b1), (a2, b2), (a3, b3), (a4, b4), (a5, b5), (a6, b6))
zipT6 (x1, x2, x3, x4, x5, x6) (y1, y2, y3, y4, y5, y6) = ((x1, y1), (x2, y2), (x3, y3), (x4, y4), (x5, y5), (x6, y6))
