{-# LANGUAGE OverloadedStrings #-}
--
module Mooncake.Teznames.Crypto
  ( sha256S
  , sha256T
  , sha256BS
  ) where
--
import qualified Data.HexString as X
import qualified Data.ByteString as B
import qualified Data.Text as T
import qualified Data.ByteString.Char8 as BC
import qualified Network.Haskoin.Crypto as HC (sha256)
import qualified Data.Serialize as S (encode)
--
sha256S :: String -> String
sha256S = str . sha256BS . strHex
--
sha256T :: T.Text -> T.Text
sha256T = X.toText . X.fromBytes . sha256BS . strHex . T.unpack
--
sha256BS :: B.ByteString -> B.ByteString
sha256BS =  S.encode . HC.sha256
--
str :: B.ByteString -> String
str = T.unpack . X.toText . X.fromBytes
--
strHex :: String -> B.ByteString
strHex = X.toBytes . X.hexString . BC.pack
--
