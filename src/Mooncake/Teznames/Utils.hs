{-# LANGUAGE OverloadedStrings #-}
--
module Mooncake.Teznames.Utils where
--
import Mooncake.Teznames.RPC
import Mooncake.Teznames.Crypto
import Mooncake.Teznames.Internal
--
import Control.Monad (liftM)
import Data.String.Utils (split)
import Data.Char (isAlphaNum)
--
isWalletAddr :: String -> Bool
isWalletAddr ('t':'z':xs) = and $ fmap isAlphaNum xs
isWalletAddr ('K':'T':xs) = and $ fmap isAlphaNum xs
isWalletAddr _ = False
--
getTeznameReady :: String -> IO (Maybe [HashedName])
getTeznameReady str = case nameTokenizer str of
  Just xs -> seqHasher xs
  Nothing -> return Nothing
--
nameTokenizer :: String -> Maybe [RawName]
nameTokenizer xs = case (reverse $ split "." xs) of
  ("tez":ys) -> return ys
  _ -> Nothing
--
seqHasher :: [RawName] -> IO (Maybe [HashedName])
seqHasher (x : xs) = do
  mpname <- liftM packed <$> tzRPCPack x
  case hasher <$> mpname of
    Just hname -> do
      mhns <- seqHasher xs
      return $ ((hname :) <$> mhns)
    Nothing    -> return Nothing
seqHasher [] = return $ Just []
--
hasher :: PackedName -> HashedName
hasher = sha256S
--
