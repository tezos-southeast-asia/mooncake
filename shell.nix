{ghc}:
with (import <nixpkgs> {});
haskell.lib.buildStackProject {
  inherit ghc;
  name = "mooncake";
  buildInputs = [
    pkg-config
    libmysqlclient
    postgresql_10
    pcre
    libsodium
    secp256k1
    zlib
    ];
  PGPASSWORD = builtins.getEnv "PGPASSWORD";
}
