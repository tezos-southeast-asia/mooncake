module Main where

import Mooncake

--
import Data.Maybe
import Database.Persist.MySQL
import qualified Options.Applicative as O

main :: IO ()
main = do
    opts <- O.execParser $ O.info (O.helper <*> cmdLineOut) O.fullDesc
    case opts of
      Server {} -> service opts

service :: Arg -> IO ()
service opts = do
  let u = fromMaybe defaultIp $ ip opts
  let p = fromMaybe defaultPort $ port opts
  let dbPt = fromMaybe defaultDBPort $ dbPort opts
  let dbHt = fromMaybe defaultDBIp $ dbIp opts
  let dbNe = fromMaybe defaultDBName $ dbName opts
  let dbPw = fromMaybe defaultDBPasswd $ dbPasswd opts
  let dbUs = fromMaybe defaultDBUser $ dbUser opts
  let pg = fromMaybe defaultPGDBConnectStr $ pgDBConnStr opts

  let connInfo = defaultConnectInfo { connectPort = fromInteger $ toInteger dbPt
                                          , connectHost = dbHt
                                          , connectPassword = dbPw
                                          , connectDatabase = dbNe
                                          , connectUser = dbUs
                                          }

  runS u p connInfo pg
